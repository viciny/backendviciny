<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// reiniciar contraseña
Route::prefix('password')->group(function(){
    Route::get('reset/{token}','AuthApi\ResetPasswordController@showResetForm')
        ->name('password.reset');

    Route::post('reset','AuthApi\ResetPasswordController@reset')
        ->name('password.update');

    Route::get('confirm', 'Utilities\UtilitiesController@viewConfirmReset');
});

//verificar email
Route::prefix('email')->group(function(){
    Route::get('verify','AuthApi\VerificationController@show')
        ->name('verification.notice');

    Route::get('verify/{id}','AuthApi\VerificationController@verify')
        ->name('verification.verify');
});

