<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// ruta  inicial, documentacion de la api
Route::get('/', 'Utilities\UtilitiesController@viewWellcome');

/*
 * rutas de autenticacion
 */
Route::prefix('auth')->group(function(){
    //ruta para el login del usuario
    Route::post('login','AuthApi\AuthController@login');
    //ruta para el registro del usuario
    Route::post('register', 'AuthApi\AuthController@register');
    //ruta para el registro del usuario
    Route::delete('logout', 'AuthApi\AuthController@logout');
    //ruta para  refrescar el token
    Route::get('refresh', 'AuthApi\AuthController@refresh');
    // ruta para obtener todo los roles del usuario
    Route::get('roles','AuthApi\AuthController@roles');
    //permite enviar un mensaje de correo para el cambio de contraseña
    Route::post('password-reset','AuthApi\ForgotPasswordController@sendResetLinkEmail');
    //validara token firebase
    Route::get('firebase/{firebase_token}','AuthApi\FirebaseController@login');
});

/*
 * Rutas del perfil del usuario autenticado
 */
Route::prefix('profile')->group(function(){
    // ruta para obtener el usuario autenticado
    Route::get('user', 'Profile\UserController@getAuthenticatedUser');
    // permite actualizar el usuario autenticado
    Route::put('update','Profile\UserController@update');
    Route::post('photo','Profile\UserController@addPhoto');
    // permite modificar la clave del usuario
    Route::put('change-password','Profile\UserController@changePassword');
    // renvia un nuevo mensaje para activar el email
    Route::get('verify-email','AuthApi\VerificationController@resend');
});

//utilites
Route::prefix('utilities')->group(function(){
    Route::get('department/{department_id}/municipalities','Utilities\DepartmentController@getMunicipalities');
    Route::apiResource('department', 'Utilities\DepartmentController')->only(['index', 'show']);
    Route::apiResource('municipality', 'Utilities\MunicipalityController')->only(['show']);
});

//Admin
Route::prefix('admin')->group(function(){
    Route::get('users','Business\AdminController@users');
    Route::get('user-events/{user_id}','Business\AdminController@userEvents');
    Route::put('update-user','Business\AdminController@editUser');
    Route::put('update-event','Business\AdminController@editEvent');
    Route::delete('delete-user/{user_id}','Business\AdminController@deleteUser');
    Route::delete('delete-event/{event_id}','Business\AdminController@deleteEVent');
    Route::get('show-user/{user_id}','Business\AdminController@showUser');
    Route::get('tickets/{tickets_order_id}','Business\AdminController@tickets');
    Route::get('tickets-orders/{event_id}','Business\AdminController@ticketsOrders');
    Route::delete('tickets-orders/{event_id}','Business\AdminController@deleteTicektsOrder');
});

Route::apiResource('type-event', 'Business\TypeEventController');
Route::get('event/popular','Business\EventController@popularEvents');
Route::get('event/my-events','Business\EventController@myEvents');
Route::get('event/{event_id}/tickes','Business\EventController@tickets');
Route::post('event/search','Business\EventController@searchEvent');
Route::post('event/{event_id}/image','Business\EventController@saveImage');
Route::apiResource('event', 'Business\EventController');
Route::get('ticket/check/{code}','Business\TicketController@check');
Route::post('ticket/confirmation-pay','Business\TicketController@confirmationPay');
Route::apiResource('ticket', 'Business\TicketController')->only(['store','destroy','index']);
Route::apiResource('favorite', 'Business\FavoriteEventController')->except(['show', 'update']);
Route::apiResource('favorite-event-type', 'Business\FavoriteTypeEventController')->except(['show', 'update','destroy']);

// test de brodcast
Route::get('test-broadcast', 'Utilities\UtilitiesController@testBroadcast@testBroadcast');