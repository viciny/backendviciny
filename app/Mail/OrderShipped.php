<?php

namespace App\Mail;

use App\Models\TicketsOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class OrderShipped extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $tickets_order;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TicketsOrder $tickets_order)
    {
        $this->tickets_order = $tickets_order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email =  $this->from(env('MAIL_USERNAME'))
            ->subject('Llaves de Viciny')
            ->subject('Eliminar viciny')
            ->view('emails.tickets')
            ->with([
                'tickets_order'=>$this->tickets_order
            ]);

        foreach ($this->tickets_order->ticketsCreated as $key=>$ticket){
            $email->attachData(QrCode::format('png')->size(500)->generate(url("ticket/check/".$ticket->code)), 'viciny_llave_'.$key.'.png', [
                'mime' => 'application/png',
            ]);
        }

        return $email;
    }
}
