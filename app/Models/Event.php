<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','organizer','address','longitude','latitude','tickets','ticket_value','start_date','finish_date','description','type_event_id','municipality_id','user_id'
    ];


    //------------------------ relaciones ---------------------

    /**
     * Get the user that owns the event.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the municipality that owns the event.
     */
    public function municipality()
    {
        return $this->belongsTo(Municipality::class);
    }

    /**
     * Get the type events that owns the event.
     */
    public function typeEvent()
    {
        return $this->belongsTo(TypeEvent::class,'type_event_id');
    }

    /**
     * The users event favorite that belong to the event.
     */
    public function usersFavorite()
    {
        return $this->belongsToMany(User::class,'favorite_events');
    }

    /**
     * Get the comments for the blog post.
     */
    public function ticketsOrders()
    {
        return $this->hasMany(TicketsOrder::class);
    }

    /**
     * Get the comments for the blog post.
     */
    public function ticketsSold()
    {
        return $this->ticketsOrders()
                ->select('tickets.*')
                ->join('tickets','tickets.tickets_order_id','=','tickets_orders.id');
    }
}
