<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tickets extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tickets_orders_id','code'
    ];


    //------------------------ relaciones ---------------------


    /**
     * Get the post that owns the comment.
     */
    public function ticketsOrder()
    {
        return $this->belongsTo(TicketsOrder::class);
    }

    /**
     * Get the post that owns the comment.
     */
    public function user()
    {
        return $this->ticketsOrder()
            ->select('users.*')
            ->join('users','users.id','=','tickets_orders.user_id');
    }

    /**
     * Get the post that owns the comment.
     */
    public function event()
    {
        return $this->ticketsOrder()
            ->select('events.*')
            ->join('events','events.id','=','tickets_orders.event_id');
    }

}
