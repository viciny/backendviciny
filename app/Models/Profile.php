<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_document','nit', 'phone','bank_name','account_bank','address','municipality_id','user_id'
    ];

    public function toArray()
    {
        return [
            "id"=>$this->id,
            "type_document"=>$this->type_document,
            "nit"=>$this->nit,
            "phone"=>$this->phone,
            "photo"=>$this->photo!=null?url('storage/'.$this->photo):$this->photo,
            "bank_name"=>$this->bank_name,
            "account_bank"=>$this->account_bank,
            "address"=>$this->address,
            "municipality"=>$this->municipality,
            "user_id"=>$this->user_id,
            "created_at"=>$this->created_at,
            "updated_at"=>$this->updated_at
        ];
    }

    //------------------------ relaciones ---------------------

    /**
     * Get the user that owns the profile.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the post that owns the comment.
     */
    public function municipality()
    {
        return $this->belongsTo(Municipality::class );
    }
}
