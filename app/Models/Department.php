<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'name'
    ];

    // relaciones del modelo
    /**
     * Get the comments for the blog post.
     */
    public function municipalities()
    {
        return $this->hasMany(Municipality::class);
    }
}
