<?php

namespace App\Models;

use App\Notifications\VerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail($this->email));
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    //------------------- relacion con el perfil --------------------------

    /**
     * Get the profile record associated with the user.
     */
    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }


    //------------------- realaciones del negocio --------------------------

    /**
     * Get the events created.
     */
    public function events()
    {
        return $this->hasMany(Event::class);
    }

    /**
     * The roles that belong to the user.
     */
    public function favoriteEvents()
    {
        return $this->belongsToMany(Event::class,'favorite_events');
    }


    /**
     * The type events that belong to the user.
     */
    public function typeEvents()
    {
        return $this->belongsToMany(TypeEvent::class,'user_type_event');
    }

    /**
     * Get the comments for the blog post.
     */
    public function ticketsOrders()
    {
        return $this->hasMany(TicketsOrder::class);
    }

    /**
     * Get the comments for the blog post.
     */
    public function tickets()
    {
        return Tickets::select('tickets.*')
            ->join('tickets_orders','tickets_orders.id','=','tickets.tickets_order_id')
            ->where('tickets_orders.user_id','=',$this->id);
    }

}
