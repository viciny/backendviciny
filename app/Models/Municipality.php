<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    protected $fillable = [
        'name','department_id'
    ];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    /**
     * Get the comments for the blog post.
     */
    public function events()
    {
        return $this->hasMany(Event::class);
    }

    /**
     * Get the users for the muncipality.
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
