<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketsOrder extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice','event_id','tickets','total'
    ];



    //------------------------ relaciones ---------------------

    /**
     * Get the comments for the blog post.
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * Get the post that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the comments for the blog post.
     */
    public function ticketsCreated()
    {
        return $this->hasMany(Tickets::class);
    }

}
