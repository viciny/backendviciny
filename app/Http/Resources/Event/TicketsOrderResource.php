<?php

namespace App\Http\Resources\Event;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketsOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "invoice"=>$this->invoice,
            "ref_payco"=>$this->ref_payco,
            "paid"=>$this->paid,
            "tickets"=>$this->tickets,
            "total"=>$this->total,
            "user"=>$this->user,
            "event_id"=>$this->event_id,
            "created_at"=>$this->created_at,
            "updated_at"=>$this->updated_at,
            "deleted_at"=>$this->deleted_at,
        ];
    }
}
