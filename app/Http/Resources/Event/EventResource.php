<?php

namespace App\Http\Resources\Event;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->municipality->department;
        return [
            'id' => $this->id,
            'name'=>$this->name,
            'organizer'=>$this->organizer,
            'address'=>$this->address,
            'longitude'=>$this->longitude,
            'latitude'=>$this->latitude,
            'start_date'=>$this->start_date,
            'tickets'=>$this->tickets,
            'ticket_value'=>$this->ticket_value,
            'numberTickets'=> $this->tickets - $this->ticketsOrders()->sum('tickets'),
            'total'=>$this->ticketsOrders()->where('tickets_orders.paid','=',true)->sum('total'),
            'finish_date'=>$this->finish_date,
            'image'=>$this->image!=null?url('storage/'.$this->image):null,
            'description'=>$this->description,
            'type_event'=>$this->typeEvent,
            'municipality'=>$this->municipality,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
