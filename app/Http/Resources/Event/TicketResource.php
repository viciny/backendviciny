<?php

namespace App\Http\Resources\Event;

use Illuminate\Http\Resources\Json\JsonResource;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $event = $this->ticketsOrder->event;
        $event->typeEvent;
        $event->municipality;
        return [
            'id'=>$this->id,
            'event'=>$this->ticketsOrder->event,
            'state'=>$this->state,
            'code_QR'=>base64_encode(QrCode::format('png')->size(400)->generate(url("ticket/check/".$this->code)))
        ];
    }
}
