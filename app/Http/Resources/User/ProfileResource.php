<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray()
    {
        return [
            "id"=>$this-> id,
            "type_document"=>$this->type_document,
            "nit"=>$this->nit,
            "photo"=>$this->photo,
            "phone"=>$this->phone,
            "bank_name"=>$this->bank_name,
            "account_bank"=>$this->account_bank,
            "address"=>$this->address,
            "municipality_id"=>$this->municipality_id,
            "user_id"=>$this->user_id,
            "created_at"=>$this->created_at,
            "updated_at"=>$this->updated_at
        ];
    }
}
