<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $idProfile = Auth::user()->profile!=null?','.Auth::user()->profile->id:'';
        return [
            'email'=>'email|confirmed|unique:users,email,'.Auth::user()->id,
            "name" => 'required|string',
            'type_document' => 'required|in:cc,ti,tp,rc,ce,ci,dni',
            'nit' => 'numeric|required|unique:profiles,nit'.$idProfile,
            'phone' => 'numeric',
            'bank_name'=>'nullable|string',
            'account_bank'=>'nullable|required_with:bank_name|numeric',
            'address' => 'string|required',
            'municipality_id'=>'required|exists:municipalities,id'
        ];
    }
}
