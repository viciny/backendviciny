<?php

namespace App\Http\Controllers\Utilities;

use App\Models\Municipality;
use App\Http\Controllers\Controller;

class MunicipalityController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $municipality = Municipality::findOrFail($id);
        return response()->json($municipality);
    }
}
