<?php

namespace App\Http\Controllers\Utilities;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UtilitiesController extends Controller
{
    function viewWellcome() {
        return view('welcome');
    }

    function viewConfirmReset() {
        return view('auth.passwords.confirmReset');
    }

    function testBroadcast(){
        broadcast(new \App\Events\PublicEvent());
        event(new \App\Events\PrivateEvent(auth()->user()->id));
        return response()->json("todo bien");
    }

}
