<?php

namespace App\Http\Controllers\AuthApi;

use App\Http\Requests\AuthApi\LoginRequest;
use App\Http\Requests\AuthApi\RegisterUserRequest;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Storage;
class AuthController extends Controller
{
    /**
     * LoginController constructor.
     */
    public function __construct(){
        $this->middleware(['jwt'])->except(['register','login']);
    }

    /**
     * permite registrar un usuario
     * @param RegisterUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterUserRequest $request){
        $user = new User($request->all());
        $user->password = bcrypt($request->password);
        $user->save();
        event(new Registered($user));
        $credentials = ['email'=>$user->email,'password'=>$request->password];
        return $this->authentication($credentials);
    }

    /**
     * permite iniciar sesion
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request){
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        return $this->authentication($credentials);

    }

    /**
     * permite cerrar la sesion  o eliminar el token
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(){
        auth()->logout();
        return response()->json([
            'message' => trans('auth.logout')
        ]);
    }

    /**
     * permite refrescar el token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * permite obtener todo los roles del usuario
     * @return mixed
     */
    function roles(){
        return auth()->user()->getRoleNames();
    }

    /**
     * @param array $credentials => emil y password para
     * la debida autenticacion
     * @return \Illuminate\Http\JsonResponse =>
     * retorna  un arrego  con el token y el usuario
     * autenticado
     */
    private function authentication(array $credentials){
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => trans('auth.failed')], 401);
        }
        return $this->respondWithToken($token);
    }


    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user(),
        ]);
    }

}
