<?php

namespace App\Http\Controllers\AuthApi;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Firebase\Auth\Token\Exception\InvalidToken;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;


class FirebaseController extends Controller
{

    private $firebase;
    public function __construct()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(base_path('firebase.json'));
        $this->firebase = (new Factory())
            ->withServiceAccount($serviceAccount)
            ->create();
    }

    public function login($token){
        $verifiedIdToken = $this->firebase->getAuth()->verifyIdToken($token);
        $uid = $verifiedIdToken->getClaim('sub');
        $userSocial = $this->firebase->getAuth()->getUser($uid);
        $user = User::where(['email' => $userSocial->email])->first();
        if($user){
            $token = auth()->login($user);
        }else{
            $user = User::create([
                "name"=>$userSocial->displayName,
                "email"=>$userSocial->email,
                "password"=>bcrypt('default'.time()),
                "email_verified_at"=>(new \DateTime())->format('d-m-Y H:i:s')
            ]);
            $user->markEmailAsVerified();
            $token = auth()->login($user);
        }
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user(),
        ]);
    }
}
