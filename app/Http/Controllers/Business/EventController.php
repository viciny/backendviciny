<?php

namespace App\Http\Controllers\Business;

use App\Http\Resources\Event\EventResource;
use App\Mail\DeleteEvent;
use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class EventController extends Controller
{
    /**
     *constructor.
     */
    public function __construct(){
        $this->middleware(['jwt'])->except(['index','show','popularEvents','searchEvent']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EventResource::collection(Event::where('start_date','<',Carbon::now())
            ->where('finish_date','>',Carbon::now())->orderBy('start_date', 'asc')->paginate(4));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['user_id'] = auth()->user()->id;
        $request->validate([
            'name'=>'required|string|max:200',
            'organizer'=>'required|string|max:200',
            'address'=>'required|string',
            'longitude'=>'required|numeric',
            'latitude'=>'required|numeric',
            'start_date'=>'required|after_or_equal:'.Carbon::now()->format('Y-m-d '),
            'tickets'=>'required|numeric|min:1',
            'ticket_value'=>'required|numeric|min:0',
            'finish_date'=>'required|after_or_equal:'.$request->start_date,
            'description'=>'string|nullable',
            'type_event_id'=>'required|exists:type_events,id',
            'municipality_id'=>'required|exists:municipalities,id',
        ]);
        return new EventResource(Event::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new EventResource(Event::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::findOrFail($id);

        if($event->user_id!=auth()->user()->id){
            return response()->json(['error' => 'Este evento no pertenece al usuario autenticado'], 403);
        }

        if($request->has('tickets')){
            if($event->ticketsOrders->count()>$request->tickets){
                return response()->json(['error' => 'Ya se encuentran mas boletas vendidas que tickets'], 403);
            }
        }

        $request->validate([
            'name'=>'string|max:200',
            'organizer'=>'string|max:200',
            'address'=>'string',
            'longitude'=>'numeric',
            'latitude'=>'numeric',
            'start_date'=>'date|after_or_equal:'.$event->start_date,
            'finish_date'=>'date|after_or_equal:'.$request->start_date,
            'tickets'=>'numeric|min:1',
            'ticket_value'=>'numeric|min:0',
            'description'=>'string',
            'type_event_id'=>'exists:type_events,id',
            'municipality_id'=>'exists:municipalities,id',
        ]);

        $event->fill($request->all());
        $event->user_id = auth()->user()->id;
        $event->save();
        return new EventResource($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        if($event->user_id==auth()->user()->id){
            if($event->ticketsOrders()->where('tickets_orders.paid','=',true)->sum('total')==0){
                Storage::disk('public')->delete($event->image);
                $event->delete();
                return new EventResource($event);
            }
            $event->by_delete = true;
            $event->save();
            Mail::to(env('MAIL_ADMIN'))->send(new DeleteEvent($event));
            return response()->json(['error' => "No podemos eliminar el viciny !. Ya se han reservado tickes. Viciny esta en la revisión de la devolución del dinero."], 403);
        }
        return response()->json(['error' => "este viciny no pertenece a este usuario."], 403);
    }

    public function saveImage(Request $request,$id){
        $request->validate([
            'image'=>'required|image'
        ]);
        $event = Event::findOrFail($id);
        if($event->user_id!=auth()->user()->id){
            return response()->json(['error' => 'El evento no pertenece a este usuario.'], 403);
        }
        if($event->image){
            Storage::disk('public')->delete($event->image);
        }
        $event->image = $request->image->store('events', 'public');
        $event->save();
        return new EventResource($event);
    }

    public function searchEvent(Request $request){
        $request->validate([
            'name'=>'nullable|string|max:200',
            'organizer'=>'nullable|string|max:200',
            'start_date'=>'after_or_equal:'.Carbon::today(),
            'finish_date'=>'after_or_equal:'.$request->start_date,
            'ticket_value'=>'numeric|min:0',
            'type_event_id'=>'exists:type_events,id',
            'municipality'=>'nullable|string|max:200',
        ]);
        $query = Event::select('events.*');
        $query->where('start_date','<',Carbon::now());
        if($request->has('start_date')&&$request->has('finish_date')){
            $query->where('finish_date','>=',$request->start_date)
            ->where('finish_date','<',$request->finish_date);
        }else if($request->has('start_date')){
            $query->where('finish_date','>=',$request->start_date);
        }else{
            $query->where('finish_date','>=',Carbon::now()->format('Y-m-d '));
        }
        if($request->has('ticket_value')){
            $query->where('ticket_value','=',$request->ticket_value);
        }
        if($request->has('name')){
            $query->where('events.name','like','%'.$request->name.'%');
        }
        if($request->has('organizer')){
            $query->where('organizer','like','%'.$request->organizer.'%');
        }
        if($request->has('type_event_id')){
            $query->where('type_event_id','=',$request->type_event_id);
        }
        if($request->has('municipality')){
            $query->join('municipalities','municipalities.id','=','events.municipality_id')
                ->where('municipalities.name','like','%'.$request->municipality.'%');
        }
        return EventResource::collection($query->orderBy('finish_date', 'asc')->paginate(6));
    }

    public function myEvents(){
        $events = auth()->user()->events()->orderBy('finish_date', 'asc')->get();
        return EventResource::collection($events);
    }

    public function popularEvents(){
        $events = Event::
        select('events.*')
            ->join('favorite_events','favorite_events.event_id','=','events.id')
            ->where('start_date','<=',Carbon::now())
            ->where('finish_date','>',Carbon::now())
            ->groupBy('favorite_events.event_id')
            ->orderBy('favorite_events.event_id','desc')
            ->limit(4)
            ->get();
        return EventResource::collection($events);
    }

    public function tickets($event_id){
        $event = Event::findOrFail($event_id);
        return $event->ticketsSold;
    }
}
