<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FavoriteTypeEventController extends Controller
{
    /**
     *constructor.
     */
    public function __construct(){
        $this->middleware(['jwt']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return auth()->user()->typeEvents;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type_events'=>'required|array',
            'type_events.*'=>'required|numeric|exists:type_events,id'
        ]);
        return auth()->user()->typeEvents()->sync($request->type_events);
    }

}
