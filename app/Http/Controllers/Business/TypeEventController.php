<?php

namespace App\Http\Controllers\Business;

use App\Http\Resources\Event\TypeEventResource;
use App\Models\TypeEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypeEventController extends Controller
{

    /**
     *constructor.
     */
    public function __construct(){
        $this->middleware(['jwt','role:administrator'])->except(['index','show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new TypeEventResource(TypeEvent::orderBy('id','desc')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:type_events|max:255'
        ]);

        return new TypeEventResource(TypeEvent::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new TypeEventResource (TypeEvent::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $typeEvent = TypeEvent::findOrFail($id);
        $request->validate([
            'name' => 'string|max:255|unique:type_events,name,'.$typeEvent->id
        ]);
        $typeEvent->fill($request->all());
        $typeEvent->save();
        return new TypeEventResource($typeEvent);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $typeEvent = TypeEvent::findOrFail($id);
        $typeEvent->delete();
        return  new TypeEventResource($typeEvent);
    }
}
