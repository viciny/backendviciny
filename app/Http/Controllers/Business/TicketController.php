<?php

namespace App\Http\Controllers\Business;

use App\Http\Resources\Event\TicketResource;
use App\Mail\OrderShipped;
use App\Models\Event;
use App\Models\Tickets;
use App\Models\TicketsOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class TicketController extends Controller
{
    /**
     *constructor.
     */
    public function __construct(){
        $this->middleware(['jwt'])->except(['confirmationPay','test']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TicketResource::collection(auth()->user()->tickets()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'event_id'=>'required|exists:events,id',
            'tickes_quantity'=>'required|numeric|min:1'
        ]);
        $event = Event::findOrFail($request->event_id);
        if($event->ticketsOrders()->sum('tickets')+$request->tickes_quantity<=$event->tickets){
            return $this->createTicketsOrder($request,$event);
        }
        return response()->json(['error' => "No se pueden comprar más tickets"], 403);
    }

    private function createTicketsOrder(Request $request,$event){
        $user = auth()->user();
        $tickets_order = new TicketsOrder();
        $tickets_order->invoice=Str::uuid();
        $tickets_order->tickets=$request->tickes_quantity;
        $tickets_order->total = $request->tickes_quantity * $event->ticket_value;
        $tickets_order->paid = $event->ticket_value==0?true:false;
        $tickets_order->user_id = $user->id;
        $tickets_order->event_id = $event->id;
        $tickets_order->save();

        if($tickets_order->paid){
            $this->createTickets($tickets_order);
        }

        return $tickets_order;
    }

    private function createTickets(TicketsOrder $tickets_order){
        for ($index=1;$index<=$tickets_order->tickets;$index++){
            $ticket = new Tickets();
            $ticket->tickets_order_id = $tickets_order->id;
            $ticket->code = Str::uuid();
            $ticket->save();
        }
        Mail::to($tickets_order->user->email)->send(new OrderShipped($tickets_order));
    }


    public function confirmationPay(Request $request){

        $p_cust_id_cliente = env("PAY_ID_CLIENTE");
        $p_key             = env("PAY_KEY");

        $request->validate([
            'x_id_invoice'=>'required|exists:tickets_orders,invoice',
        ]);

        $x_ref_payco      = $request->x_ref_payco;
        $x_transaction_id = $request->x_transaction_id;
        $x_amount         = $request->x_amount;
        $x_currency_code  = $request->x_currency_code;
        $x_signature      = $request->x_signature;

        $x_id_invoice = $request->x_id_invoice;

        $signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);



        //Validamos la firma
        if ($x_signature == $signature) {

            /*Si la firma esta bien podemos verificar los estado de la transacción*/

            $tickets_order = TicketsOrder::where('invoice','=',$x_id_invoice)->first();
            
            $x_cod_response = $request->x_cod_transaction_state;

            switch ((int) $x_cod_response) {

                case 1:
                    if(!$tickets_order->paid){
                        $tickets_order->paid = true;
                        $tickets_order->ref_payco = $x_ref_payco;
                        $this->createTickets($tickets_order);
                        //$tickets_order->save();
                    }
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    # code transacción pendiente
                    //echo 'transacción pendiente';
                    break;

                default:
                    $tickets_order->forceDelete();
                    break;
            }

        } else {
            die('Firma no valida');
        }
    }

    public function check($code){
        $ticket = Tickets::where('code',$code)->first();
        if($ticket){
            if($ticket->event->user_id==auth()->user()->id){
                $ticket->user;
                if($ticket->state){
                    $ticket->state=false;
                    $ticket->save();
                    $ticket->state=true;
                }else{
                    $ticket->state=false;
                }
                return $ticket;
            }
            return response()->json(['error' => "La llave no pertenece a este evento."], 403);
        }
        return response()->json(['error' => "La llave no es valida"], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ticket_id)
    {
        $ticket = Tickets::findOrFail($ticket_id);
        $ticket->delete();
        return $ticket;
    }


}
