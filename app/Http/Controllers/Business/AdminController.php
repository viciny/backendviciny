<?php

namespace App\Http\Controllers\Business;

use App\Http\Resources\Event\EventResource;
use App\Http\Resources\Event\TicketsOrderResource;
use App\Models\Event;
use App\Models\Profile;
use App\Models\TicketsOrder;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    /**
     *constructor.
     */
    public function __construct(){
        $this->middleware(['jwt','role:administrator']);
    }

    public function users(){
        return User::all();
    }

    public function userEvents($user_id){
        return User::findOrFail($user_id)->events;
    }

    public function showUser($user_id){
        $user = User::findOrFail($user_id);
        $user->profile;
        return $user;
    }

    public function editUser(Request $request){
        $user = User::findOrFail($request->user_id);
        $profile = $user->profile;
        $profile_id = $profile!=null?','.$profile->id:'';
        $request->validate([
            'email'=>'required|email|unique:users,email,'.$user->id,
            "name" => 'required|string',
            'type_document' => 'nullable|in:cc,ti,tp,rc,ce,ci,dni',
            'nit' => 'nullable|numeric|unique:profiles,nit'.$profile_id,
            'phone' => 'nullable|numeric',
            'bank_name'=>'nullable|string',
            'account_bank'=>'nullable|required_with:bank_name|numeric',
            'address' => 'nullable|string',
            'municipality_id'=>'nullable|exists:municipalities,id'
        ]);

        $user->fill($request->all());
        if($profile==null){
            $profile = new Profile();
        }
        $profile->fill($request->all());
        $profile->user_id = $user->id;
        $profile->save();
        $user->save();
        $user->refresh();
        return response()->json(['user'=>$user]);
    }

    public function editEvent(Request $request){
        $request->validate([
            'id' => 'required|exists:events,id',
            'name'=>'string|max:200',
            'organizer'=>'string|max:200',
            'address'=>'string',
            'longitude'=>'numeric',
            'latitude'=>'numeric',
            'start_date'=>'after_or_equal:'.Carbon::now()->format('Y-m-d '),
            'finish_date'=>'after_or_equal:'.$request->start_date,
            'tickets'=>'numeric|min:1',
            'ticket_value'=>'numeric|min:0',
            'description'=>'string',
            'url_video'=>'url',
            'type_event_id'=>'exists:type_events,id',
            'municipality_id'=>'exists:municipalities,id',
        ]);
        $event = Event::findOrFail($request->id);
        if($request->has('tickets')){
            if($event->usersTicket->count()>$request->tickets){
                return response()->json(['error' => 'Ya se encuentran mas boletas vendidas que tickets'], 422);
            }
        }
        $event->fill($request->all());
        $event->save();
        return new EventResource($event);
    }

    public function deleteUser($user_id){
        $user = User::findOrFail($user_id);
        if($user->id === auth()->user()->id){
            return response()->json(['error' => "No podemos eliminar el usuario. Autenticado."], 401);
        }
        if($user->events->count()>0){
            return response()->json(['error' => "No podemos eliminar el usuario. El usuario aun tiene eventos"], 401);
        }
        $user->delete();
        return $user;
    }

    public function deleteEVent($event_id){
        $event = Event::findOrFail($event_id);
        if($event->ticketsOrders()->where('tickets_orders.paid','=',true)->sum('total')>0){
            return response()->json(['error' => "No podemos eliminar el evento. aun tiene tickes"], 401);
        }
        Storage::disk('public')->delete($event->image);
        $event->delete();
        return $event;
    }

    public function deleteTicektsOrder($tickets_order_id){
        $order = TicketsOrder::findOrFail($tickets_order_id);
        $order->delete();
        return $order;
    }

    public function ticketsOrders($event_id){
        $event = Event::findOrFail($event_id);
        return TicketsOrderResource::collection($event->ticketsOrders);
    }

    public function tickets($tickets_order_id){
        $tickets_order = TicketsOrder::findOrFail($tickets_order_id);
        return $tickets_order->ticketsCreated;
    }
}
