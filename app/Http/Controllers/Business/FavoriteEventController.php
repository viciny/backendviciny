<?php

namespace App\Http\Controllers\Business;

use App\Http\Resources\Event\EventResource;
use App\Models\Event;
use App\Models\FavoriteEvents;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FavoriteEventController extends Controller
{
    /**
     *constructor.
     */
    public function __construct(){
        $this->middleware(['jwt']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return EventResource::collection(auth()->user()->favoriteEvents);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['user_id'] = auth()->user()->id;
        $request->validate([
            'event_id'=>'required|exists:events,id',
            'user_id' => 'required|unique_with:favorite_events,event_id'
        ]);
        return FavoriteEvents::create($request->all());
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_id)
    {
        $user = auth()->user();
        $user->favoriteEvents()->detach($event_id);
    }
}
