<?php

namespace App\Http\Controllers\Profile;

use App\Http\Requests\Profile\ProfileRequest;
use App\Http\Requests\Profile\ResetPasswordRequest;
use App\Http\Requests\Profile\UserRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use App\Models\Profile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     *constructor.
     */
    public function __construct(){
        $this->middleware(['jwt','verified']);
    }

    /**
     * usuario autenticado con el token que se pasa por la cabecera
     * del servicio http  (Authentication Bearer)
     * @return \Illuminate\Http\JsonResponse => se obtiene el
     */
    public function getAuthenticatedUser(){
        $user = auth()->user();
        $user->profile;
        return response()->json(['user'=>$user]);
    }

    /**
     * permite actualizar el usuario autenticado
     * @param UserRequest $request
     * @return mixed
     */
    public function update(UserRequest $request){
        $user = auth()->user();
        $user->fill($request->all());
        $profile = $user->profile;
        if($profile==null){
            $profile = new Profile();
        }
        $profile->fill($request->all());
        $profile->user_id = $user->id;
        $profile->save();
        $user->save();
        $user->refresh();
        return response()->json(['user'=>$user]);
    }

    public function addPhoto(Request $request){
        $request->validate(
            [
                'photo'=>'required|image'
            ]
        );
        $profile = auth()->user()->profile;
        if($profile!=null){
            if($profile->photo){
                Storage::disk('public')->delete($profile->photo);
            }
            $profile->photo = $request->photo->store('profile', 'public');
            $profile->save();
            return $profile;
        }
        return response()->json(['error' => 'El usuario no tiene perfil'], 401);
    }

    /**
     * permite modificar la clave del usuario
     * @param ResetPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ResetPasswordRequest $request){
        $user = auth()->user();

        if(Hash::check($request->old_password, $user->password)){
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json(compact('user'));
        }
        return response()->json(['errors'=>['password'=>trans('auth.failed')]],422);
    }
}
