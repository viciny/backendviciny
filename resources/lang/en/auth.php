<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'logout' => 'User logged out successfully',
    'notLogout'=>'Sorry, the user cannot be logged out',
    'notToken' => 'could_not_create_token',
    'tokenIsExpired' => 'token is expired',
    'tokenIsInvalid' => 'token is invalid',
    'tokenAbsent' => 'token absent',
    'verify' => 'The user is already enabled',
    'emailVerify' => 'The verification email was sent',
];
