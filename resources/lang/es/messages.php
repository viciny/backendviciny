<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 6/12/18
 * Time: 09:03 PM
 */

return [
    "reset_password" => "Restablecer contraseña",
    "reset_message1" => "Está recibiendo este correo electrónico porque recibimos una solicitud de restablecimiento de contraseña para su cuenta.",
    "reset_button"  => "Restablecer la contraseña",
    "reset_message2" => "Si no solicitó un restablecimiento de contraseña, no es necesario realizar ninguna otra acción.",
    "reset_confirm" => "La contraseña se modifico. gracias.",
    "verified" =>"El usuario ya se encuentra activado",
    "verifying"=>"El usuario se habilito"
];