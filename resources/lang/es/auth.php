<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'logout' => 'El usuario se desconectó con éxito',
    'notLogout'=>'Lo sentimos, el usuario no puede ser desconectado',
    'notToken' => 'No se pudo crear el token',
    'tokenIsExpired' => 'El token ha caducado',
    'tokenIsInvalid' => 'El token no es válido',
    'tokenAbsent' => 'Token ausente',
    'verify' => 'El usuario ya se encuentra habilitado',
    'emailVerify' => 'Se envio el email de verificacion',
    'not_authorized'=>'no autorizado',
];
