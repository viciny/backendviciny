<!DOCTYPE html><html><head><meta charset="utf-8"><title>Viciny</title><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"><style>@import url('https://fonts.googleapis.com/css?family=Roboto:400,700|Inconsolata|Raleway:200');.hljs-comment,.hljs-title{color:#8e908c}.hljs-variable,.hljs-attribute,.hljs-tag,.hljs-regexp,.ruby .hljs-constant,.xml .hljs-tag .hljs-title,.xml .hljs-pi,.xml .hljs-doctype,.html .hljs-doctype,.css .hljs-id,.css .hljs-class,.css .hljs-pseudo{color:#c82829}.hljs-number,.hljs-preprocessor,.hljs-pragma,.hljs-built_in,.hljs-literal,.hljs-params,.hljs-constant{color:#f5871f}.ruby .hljs-class .hljs-title,.css .hljs-rules .hljs-attribute{color:#eab700}.hljs-string,.hljs-value,.hljs-inheritance,.hljs-header,.ruby .hljs-symbol,.xml .hljs-cdata{color:#718c00}.css .hljs-hexcolor{color:#3e999f}.hljs-function,.python .hljs-decorator,.python .hljs-title,.ruby .hljs-function .hljs-title,.ruby .hljs-title .hljs-keyword,.perl .hljs-sub,.javascript .hljs-title,.coffeescript .hljs-title{color:#4271ae}.hljs-keyword,.javascript .hljs-function{color:#8959a8}.hljs{display:block;background:white;color:#4d4d4c;padding:.5em}.coffeescript .javascript,.javascript .xml,.tex .hljs-formula,.xml .javascript,.xml .vbscript,.xml .css,.xml .hljs-cdata{opacity:.5}.right .hljs-comment{color:#969896}.right .css .hljs-class,.right .css .hljs-id,.right .css .hljs-pseudo,.right .hljs-attribute,.right .hljs-regexp,.right .hljs-tag,.right .hljs-variable,.right .html .hljs-doctype,.right .ruby .hljs-constant,.right .xml .hljs-doctype,.right .xml .hljs-pi,.right .xml .hljs-tag .hljs-title{color:#c66}.right .hljs-built_in,.right .hljs-constant,.right .hljs-literal,.right .hljs-number,.right .hljs-params,.right .hljs-pragma,.right .hljs-preprocessor{color:#de935f}.right .css .hljs-rule .hljs-attribute,.right .ruby .hljs-class .hljs-title{color:#f0c674}.right .hljs-header,.right .hljs-inheritance,.right .hljs-name,.right .hljs-string,.right .hljs-value,.right .ruby .hljs-symbol,.right .xml .hljs-cdata{color:#b5bd68}.right .css .hljs-hexcolor,.right .hljs-title{color:#8abeb7}.right .coffeescript .hljs-title,.right .hljs-function,.right .javascript .hljs-title,.right .perl .hljs-sub,.right .python .hljs-decorator,.right .python .hljs-title,.right .ruby .hljs-function .hljs-title,.right .ruby .hljs-title .hljs-keyword{color:#81a2be}.right .hljs-keyword,.right .javascript .hljs-function{color:#b294bb}.right .hljs{display:block;overflow-x:auto;background:#1d1f21;color:#c5c8c6;padding:.5em;-webkit-text-size-adjust:none}.right .coffeescript .javascript,.right .javascript .xml,.right .tex .hljs-formula,.right .xml .css,.right .xml .hljs-cdata,.right .xml .javascript,.right .xml .vbscript{opacity:.5}body{color:black;background:white;font:400 14px / 1.42 'Roboto',Helvetica,sans-serif}header{border-bottom:1px solid #f2f2f2;margin-bottom:12px}h1,h2,h3,h4,h5{color:black;margin:12px 0}h1 .permalink,h2 .permalink,h3 .permalink,h4 .permalink,h5 .permalink{margin-left:0;opacity:0;transition:opacity .25s ease}h1:hover .permalink,h2:hover .permalink,h3:hover .permalink,h4:hover .permalink,h5:hover .permalink{opacity:1}.triple h1 .permalink,.triple h2 .permalink,.triple h3 .permalink,.triple h4 .permalink,.triple h5 .permalink{opacity:.15}.triple h1:hover .permalink,.triple h2:hover .permalink,.triple h3:hover .permalink,.triple h4:hover .permalink,.triple h5:hover .permalink{opacity:.15}h1{font:200 36px 'Raleway',Helvetica,sans-serif;font-size:36px}h2{font:200 36px 'Raleway',Helvetica,sans-serif;font-size:30px}h3{font-size:100%;text-transform:uppercase}h5{font-size:100%;font-weight:normal}p{margin:0 0 10px}p.choices{line-height:1.6}a{color:#428bca;text-decoration:none}li p{margin:0}hr.split{border:0;height:1px;width:100%;padding-left:6px;margin:12px auto;background-image:linear-gradient(to right, rgba(0,0,0,0) 20%, rgba(0,0,0,0.2) 51.4%, rgba(255,255,255,0.2) 51.4%, rgba(255,255,255,0) 80%)}dl dt{float:left;width:130px;clear:left;text-align:right;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;font-weight:700}dl dd{margin-left:150px}blockquote{color:rgba(0,0,0,0.5);font-size:15.5px;padding:10px 20px;margin:12px 0;border-left:5px solid #e8e8e8}blockquote p:last-child{margin-bottom:0}pre{background-color:#f5f5f5;padding:12px;border:1px solid #cfcfcf;border-radius:6px;overflow:auto}pre code{color:black;background-color:transparent;padding:0;border:none}code{color:#444;background-color:#f5f5f5;font:'Inconsolata',monospace;padding:1px 4px;border:1px solid #cfcfcf;border-radius:3px}ul,ol{padding-left:2em}table{border-collapse:collapse;border-spacing:0;margin-bottom:12px}table tr:nth-child(2n){background-color:#fafafa}table th,table td{padding:6px 12px;border:1px solid #e6e6e6}.text-muted{opacity:.5}.note,.warning{padding:.3em 1em;margin:1em 0;border-radius:2px;font-size:90%}.note h1,.warning h1,.note h2,.warning h2,.note h3,.warning h3,.note h4,.warning h4,.note h5,.warning h5,.note h6,.warning h6{font-family:200 36px 'Raleway',Helvetica,sans-serif;font-size:135%;font-weight:500}.note p,.warning p{margin:.5em 0}.note{color:black;background-color:#f0f6fb;border-left:4px solid #428bca}.note h1,.note h2,.note h3,.note h4,.note h5,.note h6{color:#428bca}.warning{color:black;background-color:#fbf1f0;border-left:4px solid #c9302c}.warning h1,.warning h2,.warning h3,.warning h4,.warning h5,.warning h6{color:#c9302c}header{margin-top:24px}nav{position:fixed;top:24px;bottom:0;overflow-y:auto}nav .resource-group{padding:0}nav .resource-group .heading{position:relative}nav .resource-group .heading .chevron{position:absolute;top:12px;right:12px;opacity:.5}nav .resource-group .heading a{display:block;color:black;opacity:.7;border-left:2px solid transparent;margin:0}nav .resource-group .heading a:hover{text-decoration:none;background-color:bad-color;border-left:2px solid black}nav ul{list-style-type:none;padding-left:0}nav ul a{display:block;font-size:13px;color:rgba(0,0,0,0.7);padding:8px 12px;border-top:1px solid #d9d9d9;border-left:2px solid transparent;overflow:hidden;text-overflow:ellipsis;white-space:nowrap}nav ul a:hover{text-decoration:none;background-color:bad-color;border-left:2px solid black}nav ul>li{margin:0}nav ul>li:first-child{margin-top:-12px}nav ul>li:last-child{margin-bottom:-12px}nav ul ul a{padding-left:24px}nav ul ul li{margin:0}nav ul ul li:first-child{margin-top:0}nav ul ul li:last-child{margin-bottom:0}nav>div>div>ul>li:first-child>a{border-top:none}.preload *{transition:none !important}.pull-left{float:left}.pull-right{float:right}.badge{display:inline-block;float:right;min-width:10px;min-height:14px;padding:3px 7px;font-size:12px;color:#000;background-color:#f2f2f2;border-radius:10px;margin:-2px 0}.badge.get{color:#70bbe1;background-color:#d9edf7}.badge.head{color:#70bbe1;background-color:#d9edf7}.badge.options{color:#70bbe1;background-color:#d9edf7}.badge.put{color:#f0db70;background-color:#fcf8e3}.badge.patch{color:#f0db70;background-color:#fcf8e3}.badge.post{color:#93cd7c;background-color:#dff0d8}.badge.delete{color:#ce8383;background-color:#f2dede}.collapse-button{float:right}.collapse-button .close{display:none;color:#428bca;cursor:pointer}.collapse-button .open{color:#428bca;cursor:pointer}.collapse-button.show .close{display:inline}.collapse-button.show .open{display:none}.collapse-content{max-height:0;overflow:hidden;transition:max-height .3s ease-in-out}nav{width:220px}.container{max-width:940px;margin-left:auto;margin-right:auto}.container .row .content{margin-left:244px;width:696px}.container .row:after{content:'';display:block;clear:both}.container-fluid nav{width:22%}.container-fluid .row .content{margin-left:24%}.container-fluid.triple nav{width:16.5%;padding-right:1px}.container-fluid.triple .row .content{position:relative;margin-left:16.5%;padding-left:24px}.middle:before,.middle:after{content:'';display:table}.middle:after{clear:both}.middle{box-sizing:border-box;width:51.5%;padding-right:12px}.right{box-sizing:border-box;float:right;width:48.5%;padding-left:12px}.right a{color:#428bca}.right h1,.right h2,.right h3,.right h4,.right h5,.right p,.right div{color:white}.right pre{background-color:#1d1f21;border:1px solid #1d1f21}.right pre code{color:#c5c8c6}.right .description{margin-top:12px}.triple .resource-heading{font-size:125%}.definition{margin-top:12px;margin-bottom:12px}.definition .method{font-weight:bold}.definition .method.get{color:#2e8ab8}.definition .method.head{color:#2e8ab8}.definition .method.options{color:#2e8ab8}.definition .method.post{color:#56b82e}.definition .method.put{color:#b8a22e}.definition .method.patch{color:#b8a22e}.definition .method.delete{color:#b82e2e}.definition .uri{word-break:break-all;word-wrap:break-word}.definition .hostname{opacity:.5}.example-names{background-color:#eee;padding:12px;border-radius:6px}.example-names .tab-button{cursor:pointer;color:black;border:1px solid #ddd;padding:6px;margin-left:12px}.example-names .tab-button.active{background-color:#d5d5d5}.right .example-names{background-color:#444}.right .example-names .tab-button{color:white;border:1px solid #666;border-radius:6px}.right .example-names .tab-button.active{background-color:#5e5e5e}#nav-background{position:fixed;left:0;top:0;bottom:0;width:16.5%;padding-right:14.4px;background-color:#fbfbfb;border-right:1px solid #f0f0f0;z-index:-1}#right-panel-background{position:absolute;right:-12px;top:-12px;bottom:-12px;width:48.6%;background-color:#333;z-index:-1}@media (max-width:1200px){nav{width:198px}.container{max-width:840px}.container .row .content{margin-left:224px;width:606px}}@media (max-width:992px){nav{width:169.4px}.container{max-width:720px}.container .row .content{margin-left:194px;width:526px}}@media (max-width:768px){nav{display:none}.container{width:95%;max-width:none}.container .row .content,.container-fluid .row .content,.container-fluid.triple .row .content{margin-left:auto;margin-right:auto;width:95%}#nav-background{display:none}#right-panel-background{width:48.6%}}.back-to-top{position:fixed;z-index:1;bottom:0;right:24px;padding:4px 8px;color:rgba(0,0,0,0.5);background-color:#f2f2f2;text-decoration:none !important;border-top:1px solid #d9d9d9;border-left:1px solid #d9d9d9;border-right:1px solid #d9d9d9;border-top-left-radius:3px;border-top-right-radius:3px}.resource-group{padding:12px;margin-bottom:12px;background-color:white;border:1px solid #d9d9d9;border-radius:6px}.resource-group h2.group-heading,.resource-group .heading a{padding:12px;margin:-12px -12px 12px -12px;background-color:#f2f2f2;border-bottom:1px solid #d9d9d9;border-top-left-radius:6px;border-top-right-radius:6px;white-space:nowrap;text-overflow:ellipsis;overflow:hidden}.triple .content .resource-group{padding:0;border:none}.triple .content .resource-group h2.group-heading,.triple .content .resource-group .heading a{margin:0 0 12px 0;border:1px solid #d9d9d9}nav .resource-group .heading a{padding:12px;margin-bottom:0}nav .resource-group .collapse-content{padding:0}.action{margin-bottom:12px;padding:12px 12px 0 12px;overflow:hidden;border:1px solid transparent;border-radius:6px}.action h4.action-heading{padding:6px 12px;margin:-12px -12px 12px -12px;border-bottom:1px solid transparent;border-top-left-radius:6px;border-top-right-radius:6px;overflow:hidden}.action h4.action-heading .name{float:right;font-weight:normal;padding:6px 0}.action h4.action-heading .method{padding:6px 12px;margin-right:12px;border-radius:3px;display:inline-block}.action h4.action-heading .method.get{color:#fff;background-color:#337ab7}.action h4.action-heading .method.head{color:#fff;background-color:#337ab7}.action h4.action-heading .method.options{color:#fff;background-color:#337ab7}.action h4.action-heading .method.put{color:#fff;background-color:#ed9c28}.action h4.action-heading .method.patch{color:#fff;background-color:#ed9c28}.action h4.action-heading .method.post{color:#fff;background-color:#5cb85c}.action h4.action-heading .method.delete{color:#fff;background-color:#d9534f}.action h4.action-heading code{color:#444;background-color:#f5f5f5;border-color:#cfcfcf;font-weight:normal;word-break:break-all;display:inline-block;margin-top:2px}.action dl.inner{padding-bottom:2px}.action .title{border-bottom:1px solid white;margin:0 -12px -1px -12px;padding:12px}.action.get{border-color:#bce8f1}.action.get h4.action-heading{color:#337ab7;background:#d9edf7;border-bottom-color:#bce8f1}.action.head{border-color:#bce8f1}.action.head h4.action-heading{color:#337ab7;background:#d9edf7;border-bottom-color:#bce8f1}.action.options{border-color:#bce8f1}.action.options h4.action-heading{color:#337ab7;background:#d9edf7;border-bottom-color:#bce8f1}.action.post{border-color:#d6e9c6}.action.post h4.action-heading{color:#5cb85c;background:#dff0d8;border-bottom-color:#d6e9c6}.action.put{border-color:#faebcc}.action.put h4.action-heading{color:#ed9c28;background:#fcf8e3;border-bottom-color:#faebcc}.action.patch{border-color:#faebcc}.action.patch h4.action-heading{color:#ed9c28;background:#fcf8e3;border-bottom-color:#faebcc}.action.delete{border-color:#ebccd1}.action.delete h4.action-heading{color:#d9534f;background:#f2dede;border-bottom-color:#ebccd1}.row{width:100% !important}nav{width:282px}.container .row .content{margin-left:310px !important;width:696px !important}nav>div>div>ul>li>a{border-top:none;font-weight:bold !important}</style></head><body class="preload"><a href="#top" class="text-muted back-to-top"><i class="fa fa-toggle-up"></i>&nbsp;Back to top</a><div class="container"><div class="row"><nav><div class="resource-group"><div class="heading"><div class="chevron"><i class="open fa fa-angle-down"></i></div><a href="#servicios-de-autenticacion">Servicios de autenticacion</a></div><div class="collapse-content"><ul><li><a href="#servicios-de-autenticacion-autenticacion">Autenticacion</a><ul><li><a href="#servicios-de-autenticacion-autenticacion-post"><span class="badge post"><i class="fa fa-plus"></i></span>Registro</a></li><li><a href="#servicios-de-autenticacion-autenticacion-post-1"><span class="badge post"><i class="fa fa-plus"></i></span>Iniciar sesión (Login)</a></li><li><a href="#servicios-de-autenticacion-autenticacion-delete"><span class="badge delete"><i class="fa fa-times"></i></span>Cerrar sesion (Logout)</a></li><li><a href="#servicios-de-autenticacion-autenticacion-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Refrescar el Token</a></li><li><a href="#servicios-de-autenticacion-autenticacion-get-1"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener roles</a></li><li><a href="#servicios-de-autenticacion-autenticacion-post-2"><span class="badge post"><i class="fa fa-plus"></i></span>Recuperar contraseña</a></li><li><a href="#servicios-de-autenticacion-autenticacion-get-2"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Login social</a></li></ul></li></ul></div></div><div class="resource-group"><div class="heading"><div class="chevron"><i class="open fa fa-angle-down"></i></div><a href="#servicios-del-perfil">Servicios del perfil</a></div><div class="collapse-content"><ul><li><a href="#servicios-del-perfil-usuario">Usuario</a><ul><li><a href="#servicios-del-perfil-usuario-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Reenviar email de activacion</a></li><li><a href="#servicios-del-perfil-usuario-get-1"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener perfil</a></li><li><a href="#servicios-del-perfil-usuario-put"><span class="badge put"><i class="fa fa-pencil"></i></span>Actualizar perfil</a></li><li><a href="#servicios-del-perfil-usuario-put-1"><span class="badge put"><i class="fa fa-pencil"></i></span>Modificar contraseña</a></li><li><a href="#servicios-del-perfil-usuario-post"><span class="badge post"><i class="fa fa-plus"></i></span>Agregar foto de perfil</a></li></ul></li></ul></div></div><div class="resource-group"><div class="heading"><div class="chevron"><i class="open fa fa-angle-down"></i></div><a href="#servicios-de-utilidad">Servicios de utilidad</a></div><div class="collapse-content"><ul><li><a href="#servicios-de-utilidad-departamento">Departamento</a><ul><li><a href="#servicios-de-utilidad-departamento-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener todos los departamentos</a></li><li><a href="#servicios-de-utilidad-departamento-get-1"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener departamento</a></li><li><a href="#servicios-de-utilidad-departamento-get-2"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener municipios</a></li><li><a href="#servicios-de-utilidad-departamento-get-3"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener municipio</a></li></ul></li></ul></div></div><div class="resource-group"><div class="heading"><div class="chevron"><i class="open fa fa-angle-down"></i></div><a href="#servicios-del-administrador">Servicios del Administrador</a></div><div class="collapse-content"><ul><li><a href="#servicios-del-administrador-administrador">Administrador</a><ul><li><a href="#servicios-del-administrador-administrador-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener todo los usuarios</a></li><li><a href="#servicios-del-administrador-administrador-get-1"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener Eventos por usuario</a></li><li><a href="#servicios-del-administrador-administrador-put"><span class="badge put"><i class="fa fa-pencil"></i></span>Actualizar usuario</a></li><li><a href="#servicios-del-administrador-administrador-put-1"><span class="badge put"><i class="fa fa-pencil"></i></span>Actualizar  evento</a></li><li><a href="#servicios-del-administrador-administrador-get-2"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener  usuario</a></li><li><a href="#servicios-del-administrador-administrador-delete"><span class="badge delete"><i class="fa fa-times"></i></span>Eliminar usuario</a></li><li><a href="#servicios-del-administrador-administrador-delete-1"><span class="badge delete"><i class="fa fa-times"></i></span>Eliminar evento</a></li><li><a href="#servicios-del-administrador-administrador-get-3"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener  los pedidos del evento</a></li><li><a href="#servicios-del-administrador-administrador-delete-2"><span class="badge delete"><i class="fa fa-times"></i></span>Eliminar pedido</a></li><li><a href="#servicios-del-administrador-administrador-get-4"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener  los tickets del pedido</a></li></ul></li></ul></div></div><div class="resource-group"><div class="heading"><div class="chevron"><i class="open fa fa-angle-down"></i></div><a href="#servicios-de-viciny">Servicios de viciny</a></div><div class="collapse-content"><ul><li><a href="#servicios-de-viciny-tipos-de-evento">Tipos de evento</a><ul><li><a href="#servicios-de-viciny-tipos-de-evento-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener todo los tipos de eventos</a></li><li><a href="#servicios-de-viciny-tipos-de-evento-post"><span class="badge post"><i class="fa fa-plus"></i></span>Crear tipo de evento</a></li><li><a href="#servicios-de-viciny-tipos-de-evento-put"><span class="badge put"><i class="fa fa-pencil"></i></span>Actualizar tipo de evento</a></li><li><a href="#servicios-de-viciny-tipos-de-evento-delete"><span class="badge delete"><i class="fa fa-times"></i></span>Eliminar tipo de evento</a></li></ul></li><li><a href="#servicios-de-viciny-tipo-de-eventos-favoritos">Tipo de eventos  favoritos</a><ul><li><a href="#servicios-de-viciny-tipo-de-eventos-favoritos-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener todo los tipos de eventos favoritos</a></li><li><a href="#servicios-de-viciny-tipo-de-eventos-favoritos-post"><span class="badge post"><i class="fa fa-plus"></i></span>Agregar evento favorito</a></li></ul></li><li><a href="#servicios-de-viciny-evento">Evento</a><ul><li><a href="#servicios-de-viciny-evento-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener todo los eventos</a></li><li><a href="#servicios-de-viciny-evento-post"><span class="badge post"><i class="fa fa-plus"></i></span>Crear evento</a></li><li><a href="#servicios-de-viciny-evento-put"><span class="badge put"><i class="fa fa-pencil"></i></span>Actualizar  evento</a></li><li><a href="#servicios-de-viciny-evento-get-1"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener evento</a></li><li><a href="#servicios-de-viciny-evento-delete"><span class="badge delete"><i class="fa fa-times"></i></span>Eliminar evento</a></li><li><a href="#servicios-de-viciny-evento-post-1"><span class="badge post"><i class="fa fa-plus"></i></span>Agregar o editar Imagen</a></li><li><a href="#servicios-de-viciny-evento-get-2"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener eventos populares</a></li><li><a href="#servicios-de-viciny-evento-post-2"><span class="badge post"><i class="fa fa-plus"></i></span>Buscar eventos</a></li><li><a href="#servicios-de-viciny-evento-get-3"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener mis eventos creados</a></li></ul></li><li><a href="#servicios-de-viciny-eventos-favoritos">Eventos favoritos</a><ul><li><a href="#servicios-de-viciny-eventos-favoritos-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener todo los eventos favoritos</a></li><li><a href="#servicios-de-viciny-eventos-favoritos-post"><span class="badge post"><i class="fa fa-plus"></i></span>Agregar evento favorito</a></li><li><a href="#servicios-de-viciny-eventos-favoritos-delete"><span class="badge delete"><i class="fa fa-times"></i></span>Eliminar evento favorito</a></li></ul></li><li><a href="#servicios-de-viciny-ticket">Ticket</a><ul><li><a href="#servicios-de-viciny-ticket-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Obtener todo los tickets del usuario</a></li><li><a href="#servicios-de-viciny-ticket-post"><span class="badge post"><i class="fa fa-plus"></i></span>Comprar ticket</a></li><li><a href="#servicios-de-viciny-ticket-post-1"><span class="badge post"><i class="fa fa-plus"></i></span>Confirmar ticket de pago (payco)</a></li><li><a href="#servicios-de-viciny-ticket-get-1"><span class="badge get"><i class="fa fa-arrow-down"></i></span>Verificar ticket</a></li><li><a href="#servicios-de-viciny-ticket-delete"><span class="badge delete"><i class="fa fa-times"></i></span>Eliminar ticket</a></li></ul></li></ul></div></div></nav><div class="content"><header><h1 id="top">Viciny</h1></header><p>Sistema  que permite realizar eventos publicos y privados para todo los usuarios</p>
<section id="servicios-de-autenticacion" class="resource-group"><h2 class="group-heading">Servicios de autenticacion <a href="#servicios-de-autenticacion" class="permalink">&para;</a></h2><div id="servicios-de-autenticacion-autenticacion" class="resource"><h3 class="resource-heading">Autenticacion <a href="#servicios-de-autenticacion-autenticacion" class="permalink">&nbsp;&para;</a></h3><p>Lista servicios encargados de la seguridad del sistema.
estos servicios  están bajo el estándar Auth2.
Auth2: es un estándar de seguridad que es controlado mediante generación de llaves(Tokens)
permitiendo que el desarrollo de la aplicación tenga una flexibilidad en la esca labilidad.</p>
<div id="servicios-de-autenticacion-autenticacion-post" class="action post"><h4 class="action-heading"><div class="name">Registro</div><a href="#servicios-de-autenticacion-autenticacion-post" class="method post">POST</a><code class="uri">/auth/register</code></h4><p>Servicio el cual permite registrar un usuario en el sistema.
El sistema recibe los datos comunes del usuarios y se debe obtener como respuesta un token de acceso.</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/auth/register</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
  "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
  "<span class="hljs-attribute">email_confirmation</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
  "<span class="hljs-attribute">password</span>": <span class="hljs-value"><span class="hljs-string">"secret"</span></span>,
  "<span class="hljs-attribute">password_confirmation</span>": <span class="hljs-value"><span class="hljs-string">"secret"</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
  "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
    <span class="hljs-string">"name"</span>,
    <span class="hljs-string">"email"</span>,
    <span class="hljs-string">"email_confirmation"</span>,
    <span class="hljs-string">"password"</span>,
    <span class="hljs-string">"password_confirmation"</span>
  ]</span>,
  "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">name</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"text"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"representa el nombre del usuario"</span>
    </span>}</span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"email"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"representa el email del usuario"</span>
    </span>}</span>,
    "<span class="hljs-attribute">email_confirmation</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"email"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"valida que la clave este bien escrita"</span>
    </span>}</span>,
    "<span class="hljs-attribute">password</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"text"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"representa la clave del acceso del usuario"</span>
    </span>}</span>,
    "<span class="hljs-attribute">password_confirmation</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"text"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"valida que la clave este bien escrita"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">access_token</span>": <span class="hljs-value"><span class="hljs-string">"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zb2NjZXJjbHViLnRlc3RcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNTU2NzYzMTYzLCJleHAiOjE1NTY3NjY3NjMsIm5iZiI6MTU1Njc2MzE2MywianRpIjoiT0hkMVhQcUp0dWY1dHkyZCIsInN1YiI6MSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.0G9Q8wvnZzjARDRtO_Ih1fiNvc13hGNbJLRo8_uLfgg"</span></span>,
  "<span class="hljs-attribute">token_type</span>": <span class="hljs-value"><span class="hljs-string">"bearer"</span></span>,
  "<span class="hljs-attribute">expires_in</span>": <span class="hljs-value"><span class="hljs-number">3600</span></span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-05-02 01:34:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-05-02 01:34:11"</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">email</span>": <span class="hljs-value">[
      <span class="hljs-string">"El correo electrónico ya ha sido registrado."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-autenticacion-autenticacion-post-1" class="action post"><h4 class="action-heading"><div class="name">Iniciar sesión (Login)</div><a href="#servicios-de-autenticacion-autenticacion-post-1" class="method post">POST</a><code class="uri">/auth/login</code></h4><p>Servicio el cual permite obtener un token de seguridad para el acceso de los recursos del sistema,
el usuario debe ingresar su email y clave.</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/auth/login</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
  "<span class="hljs-attribute">password</span>": <span class="hljs-value"><span class="hljs-string">"secret"</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
        <span class="hljs-string">"email"</span>,
        <span class="hljs-string">"password"</span>,
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">email</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"email"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el email del usuario"</span>
        </span>}</span>,
        "<span class="hljs-attribute">password</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la clave del acceso del usuario"</span>
        </span>}</span>,
    }
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">access_token</span>": <span class="hljs-value"><span class="hljs-string">"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zb2NjZXJjbHViLnRlc3RcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNTU2NzYzMTYzLCJleHAiOjE1NTY3NjY3NjMsIm5iZiI6MTU1Njc2MzE2MywianRpIjoiT0hkMVhQcUp0dWY1dHkyZCIsInN1YiI6MSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.0G9Q8wvnZzjARDRtO_Ih1fiNvc13hGNbJLRo8_uLfgg"</span></span>,
  "<span class="hljs-attribute">token_type</span>": <span class="hljs-value"><span class="hljs-string">"bearer"</span></span>,
  "<span class="hljs-attribute">expires_in</span>": <span class="hljs-value"><span class="hljs-number">3600</span></span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-05-02 01:34:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-05-02 01:34:11"</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">password</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo contraseña es obligatorio."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-autenticacion-autenticacion-delete" class="action delete"><h4 class="action-heading"><div class="name">Cerrar sesion (Logout)</div><a href="#servicios-de-autenticacion-autenticacion-delete" class="method delete">DELETE</a><code class="uri">/auth/logout</code></h4><p>Servicio el cual permite que un usuario de por terminada una sesión en el sistema.</p>
<h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname"></span>/auth/logout</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"El usuario se desconectó con éxito"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"El token no es válido"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-autenticacion-autenticacion-get" class="action get"><h4 class="action-heading"><div class="name">Refrescar el Token</div><a href="#servicios-de-autenticacion-autenticacion-get" class="method get">GET</a><code class="uri">/auth/refresh</code></h4><p>Servicio el cual permite obtener  un token nuevo mediante el token actual.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/auth/refresh</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">access_token</span>": <span class="hljs-value"><span class="hljs-string">"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zb2NjZXJjbHViLnRlc3RcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNTU2NzYzMTYzLCJleHAiOjE1NTY3NjY3NjMsIm5iZiI6MTU1Njc2MzE2MywianRpIjoiT0hkMVhQcUp0dWY1dHkyZCIsInN1YiI6MSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.0G9Q8wvnZzjARDRtO_Ih1fiNvc13hGNbJLRo8_uLfgg"</span></span>,
  "<span class="hljs-attribute">token_type</span>": <span class="hljs-value"><span class="hljs-string">"bearer"</span></span>,
  "<span class="hljs-attribute">expires_in</span>": <span class="hljs-value"><span class="hljs-number">3600</span></span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-05-02 01:34:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-05-02 01:34:11"</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"tokenIsExpired"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-autenticacion-autenticacion-get-1" class="action get"><h4 class="action-heading"><div class="name">Obtener roles</div><a href="#servicios-de-autenticacion-autenticacion-get-1" class="method get">GET</a><code class="uri">/auth/roles</code></h4><p>Servicio el cual permite obtener  todo los roles del usuario.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/auth/roles</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>[
    'administrador'
]</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"tokenIsExpired"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-autenticacion-autenticacion-post-2" class="action post"><h4 class="action-heading"><div class="name">Recuperar contraseña</div><a href="#servicios-de-autenticacion-autenticacion-post-2" class="method post">POST</a><code class="uri">/auth/password-reset</code></h4><p>Servicio el cual permite enviar un correo para recuperar la contraseña de dicho usuario.</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/auth/password-reset</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
        <span class="hljs-string">"email"</span>
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">email</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"email"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el email del usuario"</span>
        </span>}</span>,
    }
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"Se envio un correo de recuperacion"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>409</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq6098145@hotmail.com"</span></span>,
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"No se pudo enviar el correo"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-autenticacion-autenticacion-get-2" class="action get"><h4 class="action-heading"><div class="name">Login social</div><a href="#servicios-de-autenticacion-autenticacion-get-2" class="method get">GET</a><code class="uri">/auth/firebase/{firebase_token}</code></h4><p>Servicio el cual permite autenticar un usuario mediante su token de firebase.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/auth/firebase/<span class="hljs-attribute" title="firebase_token">firebase_token</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>firebase_token</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>Token de firebase.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">access_token</span>": <span class="hljs-value"><span class="hljs-string">"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9zb2NjZXJjbHViLnRlc3RcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNTU2NzYzMTYzLCJleHAiOjE1NTY3NjY3NjMsIm5iZiI6MTU1Njc2MzE2MywianRpIjoiT0hkMVhQcUp0dWY1dHkyZCIsInN1YiI6MSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.0G9Q8wvnZzjARDRtO_Ih1fiNvc13hGNbJLRo8_uLfgg"</span></span>,
  "<span class="hljs-attribute">token_type</span>": <span class="hljs-value"><span class="hljs-string">"bearer"</span></span>,
  "<span class="hljs-attribute">expires_in</span>": <span class="hljs-value"><span class="hljs-number">3600</span></span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-05-02 01:34:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-05-02 01:34:11"</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"tokenIsExpired"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div></section><section id="servicios-del-perfil" class="resource-group"><h2 class="group-heading">Servicios del perfil <a href="#servicios-del-perfil" class="permalink">&para;</a></h2><div id="servicios-del-perfil-usuario" class="resource"><h3 class="resource-heading">Usuario <a href="#servicios-del-perfil-usuario" class="permalink">&nbsp;&para;</a></h3><p>Lista servicios encargados de la manipulacion de los datos personales del usuario.</p>
<div id="servicios-del-perfil-usuario-get" class="action get"><h4 class="action-heading"><div class="name">Reenviar email de activacion</div><a href="#servicios-del-perfil-usuario-get" class="method get">GET</a><code class="uri">/profile/verify-email</code></h4><p>Servicio el cual permite enviar un mensaje al email, permitiendo  activar la cuenta del usuario.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/profile/verify-email</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"Se envio el email de verificacion"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-perfil-usuario-get-1" class="action get"><h4 class="action-heading"><div class="name">Obtener perfil</div><a href="#servicios-del-perfil-usuario-get-1" class="method get">GET</a><code class="uri">/profile/user</code></h4><p>Servicio el cual permite obtener la informacion del usuario. el perfil inicialmente puede llegar en null.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/profile/user</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:02:30"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:01:51"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:02:30"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">profile</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">type_document</span>": <span class="hljs-value"><span class="hljs-string">"cc"</span></span>,
      "<span class="hljs-attribute">nit</span>": <span class="hljs-value"><span class="hljs-string">"143034344"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-string">"78834"</span></span>,
      "<span class="hljs-attribute">account_bank</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"casa la rivera"</span></span>,
      "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:06:12"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:06:12"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>403</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Your email address is not verified."</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-perfil-usuario-put" class="action put"><h4 class="action-heading"><div class="name">Actualizar perfil</div><a href="#servicios-del-perfil-usuario-put" class="method put">PUT</a><code class="uri">/profile/update</code></h4><p>Servicio que permite actualizar la información básica del usuario.</p>
<h4>Example URI</h4><div class="definition"><span class="method put">PUT</span>&nbsp;<span class="uri"><span class="hostname"></span>/profile/update</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
  "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
  "<span class="hljs-attribute">email_confirmation</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
  "<span class="hljs-attribute">type_document</span>": <span class="hljs-value"><span class="hljs-string">"cc"</span></span>,
  "<span class="hljs-attribute">nit</span>": <span class="hljs-value"><span class="hljs-string">"143034344"</span></span>,
  "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-string">"codificadobase64....."</span></span>,
  "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-string">"78834"</span></span>,
  "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"casa la rivera"</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">optional</span>": <span class="hljs-value">[
        <span class="hljs-string">"name"</span>,
        <span class="hljs-string">"email"</span>,
        <span class="hljs-string">"account_bank"</span>
    ]</span>,
    "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
        <span class="hljs-string">"type_document"</span>,
        <span class="hljs-string">"nit"</span>,
        <span class="hljs-string">"phone"</span>,
        <span class="hljs-string">"address"</span>,
        <span class="hljs-string">"municipality_id"</span>
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">name</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el nombre del usuario"</span>
        </span>}</span>,
        "<span class="hljs-attribute">email</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"email"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el email del usuario"</span>
        </span>}</span>,
        "<span class="hljs-attribute">type_document</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text/numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el numero de identificacion"</span></span>,
            "<span class="hljs-attribute">value</span>":<span class="hljs-value">[
                <span class="hljs-string">"cc"</span> =&gt; <span class="hljs-string">"cedula de ciudadania"</span>,
                <span class="hljs-string">"ti"</span> =&gt; <span class="hljs-string">"tarjeta de identidad"</span>,
                <span class="hljs-string">"tp"</span> =&gt; <span class="hljs-string">"pasaporte"</span>,
                <span class="hljs-string">"rc"</span> =&gt; <span class="hljs-string">"registro civil"</span>,
                <span class="hljs-string">"ce"</span> =&gt; <span class="hljs-string">"cedula de extranjeria"</span>,
                <span class="hljs-string">"ci"</span> =&gt; <span class="hljs-string">"carne de identidad"</span>,
                <span class="hljs-string">"dni"</span> =&gt; <span class="hljs-string">"documento unico de identidad"</span>
            ]
        </span>}</span>,
        "<span class="hljs-attribute">nit</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text/numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el numero de identificacion"</span>
        </span>}</span>,
        "<span class="hljs-attribute">account_bank</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text/numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el numero bancario del usuario"</span>
        </span>}</span>,
        "<span class="hljs-attribute">phone</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text/numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el telefono del usuario."</span>
        </span>}</span>,
        "<span class="hljs-attribute">address</span>":<span class="hljs-value">{
              "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
              "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la direccion del usuario."</span>
          </span>}</span>,
        "<span class="hljs-attribute">municipality_id</span>":<span class="hljs-value">{
              "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
              "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identifiador del municipio"</span>
          </span>}
    </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:02:30"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:01:51"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:02:30"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">profile</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">type_document</span>": <span class="hljs-value"><span class="hljs-string">"cc"</span></span>,
      "<span class="hljs-attribute">nit</span>": <span class="hljs-value"><span class="hljs-string">"143034344"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-string">"78834"</span></span>,
      "<span class="hljs-attribute">account_bank</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"casa la rivera"</span></span>,
      "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:06:12"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:06:12"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">email</span>": <span class="hljs-value">[
      <span class="hljs-string">"correo electrónico no es un correo válido"</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-perfil-usuario-put-1" class="action put"><h4 class="action-heading"><div class="name">Modificar contraseña</div><a href="#servicios-del-perfil-usuario-put-1" class="method put">PUT</a><code class="uri">/profile/change-password</code></h4><p>Servicio que permite modificar contraseña del usuario.
el usuario debe conocer su contraseña actual para realizar la modificacion.</p>
<h4>Example URI</h4><div class="definition"><span class="method put">PUT</span>&nbsp;<span class="uri"><span class="hostname"></span>/profile/change-password</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">old_password</span>": <span class="hljs-value"><span class="hljs-string">"secret"</span></span>,
  "<span class="hljs-attribute">password</span>": <span class="hljs-value"><span class="hljs-string">"secret2"</span></span>,
  "<span class="hljs-attribute">password_confirmation</span>": <span class="hljs-value"><span class="hljs-string">"secret2"</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
        <span class="hljs-string">"old_password"</span>,
        <span class="hljs-string">"password"</span>,
        <span class="hljs-string">"password_confirmation"</span>
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">name</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">old_password</span>":<span class="hljs-value"><span class="hljs-string">"representa la contraseña actual del usuario"</span>
        </span>}</span>,
        "<span class="hljs-attribute">password</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la nueva contraseña"</span>
        </span>}
        <span class="hljs-string">"password_confirmation"</span>:{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"valida la contraseña nueva"</span>
        </span>}
    </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:02:30"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:01:51"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:02:30"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">profile</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">type_document</span>": <span class="hljs-value"><span class="hljs-string">"cc"</span></span>,
      "<span class="hljs-attribute">nit</span>": <span class="hljs-value"><span class="hljs-string">"143034344"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-string">"78834"</span></span>,
      "<span class="hljs-attribute">account_bank</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"casa la rivera"</span></span>,
      "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:06:12"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:06:12"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">old_password</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo old password es obligatorio."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-perfil-usuario-post" class="action post"><h4 class="action-heading"><div class="name">Agregar foto de perfil</div><a href="#servicios-del-perfil-usuario-post" class="method post">POST</a><code class="uri">/profile/photo</code></h4><p>Servicio que permite agregar o modificar la foto del usuario.</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/profile/photo</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">multipart/form-data</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-string">""</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
  "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
    <span class="hljs-string">"photo"</span>
  ]</span>,
  "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">photo</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"text"</span></span>,
      "<span class="hljs-attribute">old_password</span>": <span class="hljs-value"><span class="hljs-string">"representa la foto del usuario"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
  "<span class="hljs-attribute">type_document</span>": <span class="hljs-value"><span class="hljs-string">"cc"</span></span>,
  "<span class="hljs-attribute">nit</span>": <span class="hljs-value"><span class="hljs-string">"143034344"</span></span>,
  "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-string">"profile/dD3ixogrDa7HCYCdseA0gaZsZSiivbo71GQETSma.png"</span></span>,
  "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-string">"78834"</span></span>,
  "<span class="hljs-attribute">account_bank</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
  "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"casa la rivera"</span></span>,
  "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
  "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
  "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:06:12"</span></span>,
  "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:50:32"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">old_password</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo old password es obligatorio."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div></section><section id="servicios-de-utilidad" class="resource-group"><h2 class="group-heading">Servicios de utilidad <a href="#servicios-de-utilidad" class="permalink">&para;</a></h2><div id="servicios-de-utilidad-departamento" class="resource"><h3 class="resource-heading">Departamento <a href="#servicios-de-utilidad-departamento" class="permalink">&nbsp;&para;</a></h3><p>Lista servicios de utilidad para el sistema.</p>
<div id="servicios-de-utilidad-departamento-get" class="action get"><h4 class="action-heading"><div class="name">Obtener todos los departamentos</div><a href="#servicios-de-utilidad-departamento-get" class="method get">GET</a><code class="uri">/utilities/department</code></h4><p>Servicio el cual permite obtener todo los departamentos.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/utilities/department</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>[
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ANTIOQUIA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">8</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ATLÁNTICO"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">11</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"BOGOTÁ, D.C."</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">13</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"BOLÍVAR"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">15</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"BOYACÁ"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">17</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"CALDAS"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">18</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"CAQUETÁ"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">19</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"CAUCA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">20</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"CESAR"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">23</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"CÓRDOBA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">25</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"CUNDINAMARCA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">27</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"CHOCÓ"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">41</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"HUILA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"LA GUAJIRA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">47</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"MAGDALENA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">50</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"META"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">52</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"NARIÑO"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">54</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"NORTE DE SANTANDER"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"RISARALDA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">68</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"SANTANDER"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">70</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"SUCRE"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">73</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"TOLIMA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">76</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"VALLE DEL CAUCA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">81</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ARAUCA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">85</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"CASANARE"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">86</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"PUTUMAYO"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:11"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">88</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">91</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"AMAZONAS"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">94</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"GUAINÍA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">95</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"GUAVIARE"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">97</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"VAUPÉS"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">99</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"VICHADA"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span>
  </span>}
]</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-utilidad-departamento-get-1" class="action get"><h4 class="action-heading"><div class="name">Obtener departamento</div><a href="#servicios-de-utilidad-departamento-get-1" class="method get">GET</a><code class="uri">/utilities/department/{department_id}</code></h4><p>Servicio el cual permite obtener el departamento por su identificador.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/utilities/department/<span class="hljs-attribute" title="department_id">department_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>department_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del  departamento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ANTIOQUIA"</span></span>,
  "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span></span>,
  "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:10"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-utilidad-departamento-get-2" class="action get"><h4 class="action-heading"><div class="name">Obtener municipios</div><a href="#servicios-de-utilidad-departamento-get-2" class="method get">GET</a><code class="uri">/utilities/department/{department_id}/municipalities</code></h4><p>Servicio el cual permite obtener los municipios por el departamento.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/utilities/department/<span class="hljs-attribute" title="department_id">department_id</span>/municipalities</span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>department_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del  departamento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>[
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Abriaquí"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">20</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Alejandria"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:13"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:13"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">30</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Amagá"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:13"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:13"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">31</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Amalfi"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">36</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Andes"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">37</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Angelópolis"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">38</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Angostura"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">40</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Anorí"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Anzá"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">45</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Apartadó"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:14"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">57</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Arboletes"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:15"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:15"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">60</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Argelia"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:15"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:15"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">65</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:15"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:15"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">81</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Barbosa"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:16"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:16"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">91</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Bello"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:17"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:17"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">92</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Belmira"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:17"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:17"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">100</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Betania"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:17"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:17"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">102</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Betulia"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:18"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:18"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">110</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Bolívar"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:18"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:18"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">116</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Briceño"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:18"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:18"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">129</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Burítica"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:19"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:19"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">135</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Caicedo"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:19"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:19"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">144</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Caldas"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:20"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:20"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">151</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Campamento"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:20"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:20"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">162</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Caracolí"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">163</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Caramanta"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">165</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Carepa"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">168</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Carmen de Viboral"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">170</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Carolina"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">177</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Caucasia"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">178</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Cañasgordas"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:21"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">190</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Chigorodó"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:22"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:22"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">217</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Cisneros"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:24"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:24"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">222</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Cocorná"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:24"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:24"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">229</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Concepción"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:25"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:25"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">231</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Concordia"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:25"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:25"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">239</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Copacabana"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:25"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:25"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">267</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Cáceres"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:27"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:27"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">276</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Dabeiba"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:27"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:27"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">281</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Don Matías"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:27"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:27"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">285</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Ebéjico"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:28"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:28"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">286</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"El Bagre"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:28"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:28"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">329</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Entrerríos"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:30"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:30"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">330</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Envigado"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:30"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:30"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">348</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Fredonia"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:31"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:31"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">350</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Frontino"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:31"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:31"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">371</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Giraldo"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:32"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:32"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">373</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Girardota"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:32"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:32"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">377</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Granada"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:32"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:32"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">387</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Guadalupe"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:33"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:33"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">399</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Guarne"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:33"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:33"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">401</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Guatapé"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:33"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:33"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">416</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Gómez Plata"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:34"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:34"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">422</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Heliconia"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:35"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:35"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">425</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Hispania"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:35"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:35"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">437</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Itagüí"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:35"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:35"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">438</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Ituango"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:35"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:35"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">442</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Jardín"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:36"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:36"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">444</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Jericó"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:36"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:36"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">457</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"La Ceja"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:36"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:36"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">463</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"La Estrella"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:37"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:37"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">477</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"La Pintada"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:37"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:37"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">485</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"La Unión"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:38"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:38"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">504</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Liborina"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:39"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:39"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">519</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Maceo"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:40"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:40"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">538</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Marinilla"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:41"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:41"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">547</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Medellín"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:41"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:41"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">569</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Montebello"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:43"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:43"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">584</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Murindó"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:43"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:43"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">585</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Mutatá"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:43"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:43"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">589</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Nariño"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:44"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:44"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">593</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Nechí"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:44"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:44"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">594</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Necoclí"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:44"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:44"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">615</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Olaya"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:45"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:45"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">656</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Peque"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:47"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:47"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">659</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Peñol"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:47"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:47"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">689</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Pueblorrico"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:49"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:49"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">694</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Puerto Berrío"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:49"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:49"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">707</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Puerto Nare"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:49"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:49"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">716</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Puerto Triunfo"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:50"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:50"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">738</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Remedios"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:52"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:52"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">743</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Retiro"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:52"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:52"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">764</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Ríonegro"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:53"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:53"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">768</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Sabanalarga"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:53"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:53"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">772</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Sabaneta"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:54"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:54"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">781</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Salgar"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:54"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:54"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">790</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San Andrés de Cuerquía"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:54"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:54"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">800</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San Carlos"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:55"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:55"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">810</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San Francisco"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:55"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:55"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">816</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San Jerónimo"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:56"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:56"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">820</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San José de Montaña"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:56"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:56"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">831</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San Juan de Urabá"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:56"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:56"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">836</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San Luís"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:57"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:57"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">851</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San Pedro"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:57"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:57"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">855</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San Pedro de Urabá"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">857</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San Rafael"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">858</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San Roque"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">861</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"San Vicente"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">867</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Santa Bárbara"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">872</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Santa Fé de Antioquia"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:58"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">883</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Santa Rosa de Osos"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:59"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:59"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">892</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Santo Domingo"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:00"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:00"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">894</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Santuario"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:00"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:00"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">902</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Segovia"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:00"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:00"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">928</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Sonsón"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:01"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:01"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">929</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Sopetrán"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:01"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:01"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">965</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Tarazá"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:03"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:03"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">967</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Tarso"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:04"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:04"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">991</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Titiribí"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:05"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:05"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">996</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Toledo"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:06"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:06"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1017</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Turbo"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:07"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:07"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1022</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Támesis"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:07"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:07"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1031</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Uramita"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:07"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:07"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1034</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Urrao"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:08"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:08"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1037</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Valdivia"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:08"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:08"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1043</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Valparaiso"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:08"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:08"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1045</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Vegachí"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:08"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:08"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1047</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Venecia"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:08"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:08"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1054</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Vigía del Fuerte"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:09"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:09"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1081</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Yalí"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1082</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Yarumal"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1083</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Yolombó"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1084</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Yondó(Casabe)"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:10"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:10"</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1091</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Zaragoza"</span></span>,
    "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:11"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:38:11"</span>
  </span>}
]</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-utilidad-departamento-get-3" class="action get"><h4 class="action-heading"><div class="name">Obtener municipio</div><a href="#servicios-de-utilidad-departamento-get-3" class="method get">GET</a><code class="uri">/utilities/municipality/{municipality_id}</code></h4><p>Servicio el cual permite obtener el municipio.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/utilities/municipality/<span class="hljs-attribute" title="municipality_id">municipality_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>municipality_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del  municipio.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">6</span></span>,
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Agrado"</span></span>,
  "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">41</span></span>,
  "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span></span>,
  "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-14 19:37:12"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div></section><section id="servicios-del-administrador" class="resource-group"><h2 class="group-heading">Servicios del Administrador <a href="#servicios-del-administrador" class="permalink">&para;</a></h2><div id="servicios-del-administrador-administrador" class="resource"><h3 class="resource-heading">Administrador <a href="#servicios-del-administrador-administrador" class="permalink">&nbsp;&para;</a></h3><p>Lista de servicios utilies para el administrador del sistema.</p>
<div id="servicios-del-administrador-administrador-get" class="action get"><h4 class="action-heading"><div class="name">Obtener todo los usuarios</div><a href="#servicios-del-administrador-administrador-get" class="method get">GET</a><code class="uri">/admin/users</code></h4><p>Servicio el cual permite obtener todo los usuarios del sistema.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/admin/users</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>[
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-27 02:40:48"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-27 02:40:48"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"santiago"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcperdomoq@uqvirtual.edu.co"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-27 03:06:08"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-27 03:06:08"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-31 21:38:08"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>}
]</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-administrador-administrador-get-1" class="action get"><h4 class="action-heading"><div class="name">Obtener Eventos por usuario</div><a href="#servicios-del-administrador-administrador-get-1" class="method get">GET</a><code class="uri">/admin/user-events/{user_id}</code></h4><p>Servicio el cual permite obtener todo los eventos de un usuario.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/admin/user-events/<span class="hljs-attribute" title="user_id">user_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>user_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del usuario.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>[
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
    "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
    "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
    "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">2000</span></span>,
    "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12 10:55:00"</span></span>,
    "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12 11:55:00"</span></span>,
    "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
    "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
    "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-27 02:43:36"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-29 18:38:47"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"evento"</span></span>,
    "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"perdomo"</span></span>,
    "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"la rivera"</span></span>,
    "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">22</span></span>,
    "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">12000</span></span>,
    "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-27 22:15:00"</span></span>,
    "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-31 22:15:00"</span></span>,
    "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">""</span></span>,
    "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"El Festival Nacional de la Música Colombiana se creó el 21 de marzo de 1987, en homenaje al dueto Garzón y Collazos y con el propósito de resaltar, impulsar y estimular las manifestaciones artísticas y culturales de los diferentes aires tradicionales de la región Andina, así mismo consolidar a Ibagué como ciudad musical de Colombia."</span></span>,
    "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-27 03:16:52"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-27 03:16:52"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"evento"</span></span>,
    "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"perdomo"</span></span>,
    "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"la rivera"</span></span>,
    "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">22</span></span>,
    "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">3000</span></span>,
    "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-01 19:34:00"</span></span>,
    "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-04 19:34:00"</span></span>,
    "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"dfadga dfadfasfafd afafas"</span></span>,
    "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">15</span></span>,
    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">107</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-28 00:34:45"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-28 00:34:45"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>}
]</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-administrador-administrador-put" class="action put"><h4 class="action-heading"><div class="name">Actualizar usuario</div><a href="#servicios-del-administrador-administrador-put" class="method put">PUT</a><code class="uri">/admin/update-user</code></h4><p>Servicio que permite actualizar un usuario.</p>
<h4>Example URI</h4><div class="definition"><span class="method put">PUT</span>&nbsp;<span class="uri"><span class="hostname"></span>/admin/update-user</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    "<span class="hljs-attribute">user_id</span>":<span class="hljs-value"><span class="hljs-number">2</span>
    <span class="hljs-string">"email"</span>:<span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">name</span>":<span class="hljs-value"><span class="hljs-string">"santiago"</span></span>,
    "<span class="hljs-attribute">type_document</span>":<span class="hljs-value"><span class="hljs-string">"cc"</span></span>,
    "<span class="hljs-attribute">nit</span>":<span class="hljs-value"><span class="hljs-string">"143034344"</span></span>,
    "<span class="hljs-attribute">phone</span>":<span class="hljs-value"><span class="hljs-string">"78834"</span></span>,
    "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">address</span>":<span class="hljs-value"><span class="hljs-string">"casa la rivera"</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">optional</span>": <span class="hljs-value">[
        <span class="hljs-string">"name"</span>,
        <span class="hljs-string">"email"</span>,
        <span class="hljs-string">"account_bank"</span>
    ]</span>,
    "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
        <span class="hljs-string">"user_id"</span>,
        <span class="hljs-string">"type_document"</span>,
        <span class="hljs-string">"nit"</span>,
        <span class="hljs-string">"phone"</span>,
        <span class="hljs-string">"address"</span>,
        <span class="hljs-string">"municipality_id"</span>
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">user_id</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identificador del usuario"</span>
        </span>}</span>,
        "<span class="hljs-attribute">name</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el nombre del usuario"</span>
        </span>}</span>,
        "<span class="hljs-attribute">email</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"email"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el email del usuario"</span>
        </span>}</span>,
        "<span class="hljs-attribute">type_document</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text/numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el numero de identificacion"</span></span>,
            "<span class="hljs-attribute">value</span>":<span class="hljs-value">[
                <span class="hljs-string">"cc"</span> =&gt; <span class="hljs-string">"cedula de ciudadania"</span>,
                <span class="hljs-string">"ti"</span> =&gt; <span class="hljs-string">"tarjeta de identidad"</span>,
                <span class="hljs-string">"tp"</span> =&gt; <span class="hljs-string">"pasaporte"</span>,
                <span class="hljs-string">"rc"</span> =&gt; <span class="hljs-string">"registro civil"</span>,
                <span class="hljs-string">"ce"</span> =&gt; <span class="hljs-string">"cedula de extranjeria"</span>,
                <span class="hljs-string">"ci"</span> =&gt; <span class="hljs-string">"carne de identidad"</span>,
                <span class="hljs-string">"dni"</span> =&gt; <span class="hljs-string">"documento unico de identidad"</span>
            ]
        </span>}</span>,
        "<span class="hljs-attribute">nit</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text/numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el numero de identificacion"</span>
        </span>}</span>,
        "<span class="hljs-attribute">account_bank</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text/numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el numero bancario del usuario"</span>
        </span>}</span>,
        "<span class="hljs-attribute">phone</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text/numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el telefono del usuario."</span>
        </span>}</span>,
        "<span class="hljs-attribute">address</span>":<span class="hljs-value">{
              "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
              "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la direccion del usuario."</span>
          </span>}</span>,
        "<span class="hljs-attribute">municipality_id</span>":<span class="hljs-value">{
              "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
              "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identifiador del municipio"</span>
          </span>}
    </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:02:30"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:01:51"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:02:30"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">profile</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">type_document</span>": <span class="hljs-value"><span class="hljs-string">"cc"</span></span>,
      "<span class="hljs-attribute">nit</span>": <span class="hljs-value"><span class="hljs-string">"143034344"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-string">"78834"</span></span>,
      "<span class="hljs-attribute">account_bank</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"casa la rivera"</span></span>,
      "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:06:12"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:06:12"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">email</span>": <span class="hljs-value">[
      <span class="hljs-string">"correo electrónico no es un correo válido"</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-administrador-administrador-put-1" class="action put"><h4 class="action-heading"><div class="name">Actualizar  evento</div><a href="#servicios-del-administrador-administrador-put-1" class="method put">PUT</a><code class="uri">/admin/update-event</code></h4><p>Servicio que permite actualizar la información de un evento.</p>
<h4>Example URI</h4><div class="definition"><span class="method put">PUT</span>&nbsp;<span class="uri"><span class="hostname"></span>/admin/update-event</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">event_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
  "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
  "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"direccion"</span></span>,
  "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 10:55"</span></span>,
  "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 11:55"</span></span>,
  "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
  "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">2000</span></span>,
  "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
  "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
  "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
  "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">66</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">required</span>":<span class="hljs-value">[
        <span class="hljs-string">"event_id"</span>
    ]</span>,
    "<span class="hljs-attribute">optional</span>":<span class="hljs-value">[
        <span class="hljs-string">"name"</span>,
        <span class="hljs-string">"organizer"</span>,
        <span class="hljs-string">"address"</span>,
        <span class="hljs-string">"start_date"</span>,
        <span class="hljs-string">"finish_date"</span>,
        <span class="hljs-string">"tickets"</span>,
        <span class="hljs-string">"ticket_value"</span>,
        <span class="hljs-string">"type_event_id"</span>,
        <span class="hljs-string">"description"</span>,
        <span class="hljs-string">"url_youtube"</span>,
        <span class="hljs-string">"municipality_id"</span>
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">event_id</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identificador del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">name</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el nombre de evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">organizer</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el nombre de la organización"</span>
        </span>}</span>,
        "<span class="hljs-attribute">address</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la direccion del evento (map)"</span>
        </span>}</span>,
        "<span class="hljs-attribute">start_date</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"date"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la fecha inicial del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">finish_date</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"date"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la fecha de finalización del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">tickets</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el numero de tickets del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">ticket_value</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el valor del ticket"</span>
        </span>}</span>,
        "<span class="hljs-attribute">type_event_id</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identificador de tipo de evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">municipality_id</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identificador del municipio"</span>
        </span>}</span>,
        "<span class="hljs-attribute">description</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"string"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la descripción del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">url_youtube</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"url"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la url del video de youtube sobre el evento"</span>
        </span>}</span>,
    }
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
    "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
    "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"direccion"</span></span>,
    "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 10:55"</span></span>,
    "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">20000</span></span>,
    "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 11:55"</span></span>,
    "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/storage/events/q0yVjMlb8LwYfi2LJUvLxRpI1X50hc78CKvgEeEK.png"</span></span>,
    "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
    "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
    "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
    </span>}</span>,
    "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
      "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
      </span>}
    </span>}</span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:06:12.000000Z"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:33:50.000000Z"</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-administrador-administrador-get-2" class="action get"><h4 class="action-heading"><div class="name">Obtener  usuario</div><a href="#servicios-del-administrador-administrador-get-2" class="method get">GET</a><code class="uri">/admin/show-event/{user_id}</code></h4><p>Servicio que permite obtener el usuario por su identificador.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/admin/show-event/<span class="hljs-attribute" title="user_id">user_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>user_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del evento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:02:30"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:01:51"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:02:30"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">profile</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">type_document</span>": <span class="hljs-value"><span class="hljs-string">"cc"</span></span>,
      "<span class="hljs-attribute">nit</span>": <span class="hljs-value"><span class="hljs-string">"143034344"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-string">"78834"</span></span>,
      "<span class="hljs-attribute">account_bank</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"casa la rivera"</span></span>,
      "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:06:12"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-25 18:06:12"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-administrador-administrador-delete" class="action delete"><h4 class="action-heading"><div class="name">Eliminar usuario</div><a href="#servicios-del-administrador-administrador-delete" class="method delete">DELETE</a><code class="uri">/admin/delete-user/{user_id}</code></h4><p>Servicio que permite eliminar un usuario, el usuario no debe tener  eventos.</p>
<h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname"></span>/admin/delete-user/<span class="hljs-attribute" title="user_id">user_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>user_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del usuario.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>401</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"No podemos eliminar el usuario. aun tiene eventos"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-administrador-administrador-delete-1" class="action delete"><h4 class="action-heading"><div class="name">Eliminar evento</div><a href="#servicios-del-administrador-administrador-delete-1" class="method delete">DELETE</a><code class="uri">/admin/delete-event/{event_id}</code></h4><p>Servicio que permite eliminar un evento, el evento no debe tener  tickes.</p>
<h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname"></span>/admin/delete-event/<span class="hljs-attribute" title="event_id">event_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>event_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del evento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
  "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
  "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
  "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
  "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">2000</span></span>,
  "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12 10:55:00"</span></span>,
  "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12 11:55:00"</span></span>,
  "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
  "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
  "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
  "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
  "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
  "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
  "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-27 02:43:36"</span></span>,
  "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-31 22:54:54"</span></span>,
  "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-31 22:54:54"</span></span>,
  "<span class="hljs-attribute">users_ticket</span>": <span class="hljs-value">[]
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>401</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"No podemos eliminar el evento. aun tiene tickes"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-administrador-administrador-get-3" class="action get"><h4 class="action-heading"><div class="name">Obtener  los pedidos del evento</div><a href="#servicios-del-administrador-administrador-get-3" class="method get">GET</a><code class="uri">/admin/tickets-orders/{event_id}</code></h4><p>Servicio que permite obtener el pedido de tickets para el evento.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/admin/tickets-orders/<span class="hljs-attribute" title="event_id">event_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>event_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del evento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">13</span></span>,
      "<span class="hljs-attribute">invoice</span>": <span class="hljs-value"><span class="hljs-string">"1f735e5f-f9c2-4d6a-912c-d2a1f58e0297"</span></span>,
      "<span class="hljs-attribute">ref_payco</span>": <span class="hljs-value"><span class="hljs-number">15363110</span></span>,
      "<span class="hljs-attribute">paid</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">5000</span></span>,
      "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">8</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Juan carlos perdomo"</span></span>,
        "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcperdomoq@uqvirtual.edu.co"</span></span>,
        "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:36:42"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:36:42"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-22 13:55:46"</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">event_id</span>": <span class="hljs-value"><span class="hljs-number">205</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-21T04:40:27.000000Z"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-21T23:51:54.000000Z"</span></span>,
      "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-administrador-administrador-delete-2" class="action delete"><h4 class="action-heading"><div class="name">Eliminar pedido</div><a href="#servicios-del-administrador-administrador-delete-2" class="method delete">DELETE</a><code class="uri">/admin/tickets-orders/{tickets_order_id}</code></h4><p>Servicio que permite eliminar un evento, el evento no debe tener  tickes.</p>
<h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname"></span>/admin/tickets-orders/<span class="hljs-attribute" title="tickets_order_id">tickets_order_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>tickets_order_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del pedido.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">13</span></span>,
  "<span class="hljs-attribute">invoice</span>": <span class="hljs-value"><span class="hljs-string">"1f735e5f-f9c2-4d6a-912c-d2a1f58e0297"</span></span>,
  "<span class="hljs-attribute">ref_payco</span>": <span class="hljs-value"><span class="hljs-number">15363110</span></span>,
  "<span class="hljs-attribute">paid</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
  "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
  "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">5000</span></span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">8</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Juan carlos perdomo"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcperdomoq@uqvirtual.edu.co"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:36:42"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:36:42"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-22 13:55:46"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>}</span>,
  "<span class="hljs-attribute">event_id</span>": <span class="hljs-value"><span class="hljs-number">205</span></span>,
  "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-21T04:40:27.000000Z"</span></span>,
  "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-21T23:51:54.000000Z"</span></span>,
  "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-del-administrador-administrador-get-4" class="action get"><h4 class="action-heading"><div class="name">Obtener  los tickets del pedido</div><a href="#servicios-del-administrador-administrador-get-4" class="method get">GET</a><code class="uri">/admin/tickets/{tickets_order_id}</code></h4><p>Servicio que permite obtener los tickets del pedido.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/admin/tickets/<span class="hljs-attribute" title="tickets_order_id">tickets_order_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>tickets_order_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del pedido.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>[
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">101</span></span>,
    "<span class="hljs-attribute">tickets_order_id</span>": <span class="hljs-value"><span class="hljs-number">13</span></span>,
    "<span class="hljs-attribute">state</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">code</span>": <span class="hljs-value"><span class="hljs-string">"6c19c9d7-bb10-4c9e-945e-8b207289bd38"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-22 01:12:03"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-22 01:12:03"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">107</span></span>,
    "<span class="hljs-attribute">tickets_order_id</span>": <span class="hljs-value"><span class="hljs-number">13</span></span>,
    "<span class="hljs-attribute">state</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">code</span>": <span class="hljs-value"><span class="hljs-string">"19ec4a30-43bc-4a4a-9481-86aafef5b804"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-22 01:25:37"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-22 01:25:37"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>}
]</code></pre><div style="height: 1px;"></div></div></div></div></div></section><section id="servicios-de-viciny" class="resource-group"><h2 class="group-heading">Servicios de viciny <a href="#servicios-de-viciny" class="permalink">&para;</a></h2><div id="servicios-de-viciny-tipos-de-evento" class="resource"><h3 class="resource-heading">Tipos de evento <a href="#servicios-de-viciny-tipos-de-evento" class="permalink">&nbsp;&para;</a></h3><p>Lista de servicios para el tipo de evento.</p>
<div id="servicios-de-viciny-tipos-de-evento-get" class="action get"><h4 class="action-heading"><div class="name">Obtener todo los tipos de eventos</div><a href="#servicios-de-viciny-tipos-de-evento-get" class="method get">GET</a><code class="uri">/type-event</code></h4><p>Servicio el cual permite obtener todo los tipos de eventos</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/type-event</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Fiesta"</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-12-28 15:27:44"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-12-28 15:27:44"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Acampada"</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-12-28 15:27:44"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-12-28 15:27:44"</span>
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-tipos-de-evento-post" class="action post"><h4 class="action-heading"><div class="name">Crear tipo de evento</div><a href="#servicios-de-viciny-tipos-de-evento-post" class="method post">POST</a><code class="uri">/type-event</code></h4><p>Servicio el cual permite crear un tipo de evento.</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/type-event</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Acampada"</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
  "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
    <span class="hljs-string">"name"</span>
  ]</span>,
  "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">name</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"text"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"representa el nombre del tipo de evento"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Acampada"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-12-28 19:58:03"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-12-28 19:58:03"</span></span>,
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">name</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo nombre es obligatorio."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-tipos-de-evento-put" class="action put"><h4 class="action-heading"><div class="name">Actualizar tipo de evento</div><a href="#servicios-de-viciny-tipos-de-evento-put" class="method put">PUT</a><code class="uri">/type-event/{typeEvent_id}</code></h4><p>Servicio que permite actualizar la información de un tipo de evento.</p>
<h4>Example URI</h4><div class="definition"><span class="method put">PUT</span>&nbsp;<span class="uri"><span class="hostname"></span>/type-event/<span class="hljs-attribute" title="typeEvent_id">typeEvent_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>typeEvent_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del tipo de evento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Acampada"</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
  "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
    <span class="hljs-string">"name"</span>
  ]</span>,
  "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">name</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"text"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"representa el nombre del tipo de evento"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Acampada"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-12-28 19:58:03"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-12-28 19:58:03"</span></span>,
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">name</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo nombre es obligatorio."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-tipos-de-evento-delete" class="action delete"><h4 class="action-heading"><div class="name">Eliminar tipo de evento</div><a href="#servicios-de-viciny-tipos-de-evento-delete" class="method delete">DELETE</a><code class="uri">/type-event/{typeEvent_id}</code></h4><p>Servicio que permite eliminar el tipo de evento</p>
<h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname"></span>/type-event/<span class="hljs-attribute" title="typeEvent_id">typeEvent_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>typeEvent_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del tipo de evento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Acampada"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-12-28 19:58:03"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2019-12-28 19:58:03"</span></span>,
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="servicios-de-viciny-tipo-de-eventos-favoritos" class="resource"><h3 class="resource-heading">Tipo de eventos  favoritos <a href="#servicios-de-viciny-tipo-de-eventos-favoritos" class="permalink">&nbsp;&para;</a></h3><p>Lista de servicios para el control de los tipo de ventos favoritos.</p>
<div id="servicios-de-viciny-tipo-de-eventos-favoritos-get" class="action get"><h4 class="action-heading"><div class="name">Obtener todo los tipos de eventos favoritos</div><a href="#servicios-de-viciny-tipo-de-eventos-favoritos-get" class="method get">GET</a><code class="uri">/favorite-event-type</code></h4><p>Servicio el cual permite obtener todo los  tipos de eventos favoritos del usuario.</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/favorite-event-type</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>[
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">6</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Artes escénicas y visuales"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-26 18:07:24"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-26 18:07:24"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">pivot</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">6</span>
    </span>}
  </span>},
  {
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">7</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Moda"</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-26 18:07:24"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-26 18:07:24"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">pivot</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">7</span>
    </span>}
  </span>}
]</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-tipo-de-eventos-favoritos-post" class="action post"><h4 class="action-heading"><div class="name">Agregar evento favorito</div><a href="#servicios-de-viciny-tipo-de-eventos-favoritos-post" class="method post">POST</a><code class="uri">/favorite</code></h4><p>Servicio el cual permite agregar un evento como favorito.</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/favorite</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">type_events</span>": <span class="hljs-value">[
    <span class="hljs-number">2</span>,
    <span class="hljs-number">4</span>,
    <span class="hljs-number">6</span>
  ]
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
        <span class="hljs-string">"type_events"</span>,
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">type_events</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"array"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la lista de identificadores de tipo de evento"</span>
        </span>}
    </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">attached</span>": <span class="hljs-value">[
    <span class="hljs-number">2</span>,
    <span class="hljs-number">4</span>
  ]</span>,
  "<span class="hljs-attribute">detached</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">1</span>": <span class="hljs-value"><span class="hljs-number">7</span></span>,
    "<span class="hljs-attribute">2</span>": <span class="hljs-value"><span class="hljs-number">1</span>
  </span>}</span>,
  "<span class="hljs-attribute">updated</span>": <span class="hljs-value">[]
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">type_events</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo type events es obligatorio."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="servicios-de-viciny-evento" class="resource"><h3 class="resource-heading">Evento <a href="#servicios-de-viciny-evento" class="permalink">&nbsp;&para;</a></h3><p>Lista de servicios para el control de los eventos.</p>
<div id="servicios-de-viciny-evento-get" class="action get"><h4 class="action-heading"><div class="name">Obtener todo los eventos</div><a href="#servicios-de-viciny-evento-get" class="method get">GET</a><code class="uri">/event</code></h4><p>Servicio el cual permite obtener todo los  eventos</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/event</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Fiesta Perdomo"</span></span>,
      "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
      "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
      "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
      "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">20000</span></span>,
      "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/storage/events/q0yVjMlb8LwYfi2LJUvLxRpI1X50hc78CKvgEeEK.png"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
      "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
      "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Otro"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">65</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
        "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ANTIOQUIA"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:00"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:00"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:06:12.000000Z"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:24:18.000000Z"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
      "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
      "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
      "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
      "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
        "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span>
    </span>}
  ]</span>,
  "<span class="hljs-attribute">links</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">first</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event?page=1"</span></span>,
    "<span class="hljs-attribute">last</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event?page=1"</span></span>,
    "<span class="hljs-attribute">prev</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">next</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>}</span>,
  "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">2</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-evento-post" class="action post"><h4 class="action-heading"><div class="name">Crear evento</div><a href="#servicios-de-viciny-evento-post" class="method post">POST</a><code class="uri">/event</code></h4><p>Servicio el cual permite crear un evento.</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/event</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
  "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
  "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
  "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
  "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
  "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 10:55"</span></span>,
  "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 11:55"</span></span>,
  "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
  "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">2000</span></span>,
  "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
  "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
  "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
  "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">66</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
        <span class="hljs-string">"name"</span>,
        <span class="hljs-string">"organizer"</span>,
        <span class="hljs-string">"address"</span>,
        <span class="hljs-string">"longitude"</span>,
        <span class="hljs-string">"latitude"</span>,
        <span class="hljs-string">"start_date"</span>,
        <span class="hljs-string">"finish_date"</span>,
        <span class="hljs-string">"tickets"</span>,
        <span class="hljs-string">"ticket_value"</span>,
        <span class="hljs-string">"type_event_id"</span>,
        <span class="hljs-string">"municipality_id"</span>
    ]</span>,
    "<span class="hljs-attribute">optional</span>":<span class="hljs-value">[
        <span class="hljs-string">"description"</span>,
        <span class="hljs-string">"url_video"</span>
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">name</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el nombre de evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">organizer</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el nombre de la organización"</span>
        </span>}</span>,
        "<span class="hljs-attribute">address</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la direccion del evento (map)"</span>
        </span>}</span>,
        "<span class="hljs-attribute">longitude</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"float"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa longitud en el mapa"</span>
        </span>}</span>,
        "<span class="hljs-attribute">latitude</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"float"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la latitud en el mapa"</span>
        </span>}</span>,
        "<span class="hljs-attribute">start_date</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"date"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la fecha inicial del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">finish_date</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"date"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la fecha de finalización del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">tickets</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el numero de tickets del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">ticket_value</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el valor del ticket"</span>
        </span>}</span>,
        "<span class="hljs-attribute">type_event_id</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identificador de tipo de evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">municipality_id</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identificador  del municipio"</span>
        </span>}</span>,
        "<span class="hljs-attribute">description</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"string"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la descripción del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">url_video</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"url"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la url del video de youtube sobre el evento"</span>
        </span>}</span>,
    }
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
    "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
    "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
    "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
    "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
    "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 10:55"</span></span>,
    "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">20000</span></span>,
    "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 11:55"</span></span>,
    "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
    "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
    "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
    </span>}</span>,
    "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
      "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
      </span>}
    </span>}</span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">name</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo nombre es obligatorio."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-evento-put" class="action put"><h4 class="action-heading"><div class="name">Actualizar  evento</div><a href="#servicios-de-viciny-evento-put" class="method put">PUT</a><code class="uri">/event/{event_id}</code></h4><p>Servicio que permite actualizar la información de un evento.</p>
<h4>Example URI</h4><div class="definition"><span class="method put">PUT</span>&nbsp;<span class="uri"><span class="hostname"></span>/event/<span class="hljs-attribute" title="event_id">event_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>event_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del evento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
  "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
  "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"direccion"</span></span>,
  "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 10:55"</span></span>,
  "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 11:55"</span></span>,
  "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
  "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">2000</span></span>,
  "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
  "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
  "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
  "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
  "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
  "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">66</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">optional</span>":<span class="hljs-value">[
        <span class="hljs-string">"name"</span>,
        <span class="hljs-string">"organizer"</span>,
        <span class="hljs-string">"address"</span>,
        <span class="hljs-string">"longitude"</span>,
        <span class="hljs-string">"latitude"</span>,
        <span class="hljs-string">"start_date"</span>,
        <span class="hljs-string">"finish_date"</span>,
        <span class="hljs-string">"tickets"</span>,
        <span class="hljs-string">"ticket_value"</span>,
        <span class="hljs-string">"type_event_id"</span>,
        <span class="hljs-string">"description"</span>,
        <span class="hljs-string">"url_youtube"</span>,
        <span class="hljs-string">"municipality_id"</span>
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">name</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el nombre de evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">organizer</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el nombre de la organización"</span>
        </span>}</span>,
        "<span class="hljs-attribute">address</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la direccion del evento (map)"</span>
        </span>}</span>,
        "<span class="hljs-attribute">longitude</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"float"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa longitud en el mapa"</span>
        </span>}</span>,
        "<span class="hljs-attribute">latitude</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"float"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la latitud en el mapa"</span>
        </span>}</span>,
        "<span class="hljs-attribute">start_date</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"date"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la fecha inicial del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">finish_date</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"date"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la fecha de finalización del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">tickets</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el numero de tickets del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">ticket_value</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el valor del ticket"</span>
        </span>}</span>,
        "<span class="hljs-attribute">type_event_id</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identificador de tipo de evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">municipality_id</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identificador del municipio"</span>
        </span>}</span>,
        "<span class="hljs-attribute">description</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"string"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la descripción del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">url_youtube</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"url"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la url del video de youtube sobre el evento"</span>
        </span>}</span>,
    }
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
    "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
    "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"direccion"</span></span>,
    "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
    "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
    "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 10:55"</span></span>,
    "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">20000</span></span>,
    "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 11:55"</span></span>,
    "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/storage/events/q0yVjMlb8LwYfi2LJUvLxRpI1X50hc78CKvgEeEK.png"</span></span>,
    "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
    "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
    "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
    </span>}</span>,
    "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
      "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
      </span>}
    </span>}</span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:06:12.000000Z"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:33:50.000000Z"</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">name</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo nombre debe ser una cadena de caracteres."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-evento-get-1" class="action get"><h4 class="action-heading"><div class="name">Obtener evento</div><a href="#servicios-de-viciny-evento-get-1" class="method get">GET</a><code class="uri">/event/{event_id}</code></h4><p>Servicio que permite obtener la informacion del evento</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/event/<span class="hljs-attribute" title="event_id">event_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>event_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del  evento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
    "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
    "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"direccion"</span></span>,
    "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
    "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
    "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
    "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">2000</span></span>,
    "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
    "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/storage/events/q0yVjMlb8LwYfi2LJUvLxRpI1X50hc78CKvgEeEK.png"</span></span>,
    "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
    "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
    "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
    </span>}</span>,
    "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
      "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
      </span>}
    </span>}</span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:06:12.000000Z"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:33:50.000000Z"</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-evento-delete" class="action delete"><h4 class="action-heading"><div class="name">Eliminar evento</div><a href="#servicios-de-viciny-evento-delete" class="method delete">DELETE</a><code class="uri">/event/{event_id}</code></h4><p>Servicio que permite eliminar el evento</p>
<h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname"></span>/event/<span class="hljs-attribute" title="event_id">event_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>event_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del evento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
    "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
    "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"direccion"</span></span>,
    "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
    "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
    "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
    "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">20000</span></span>,
    "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
    "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/storage/events/q0yVjMlb8LwYfi2LJUvLxRpI1X50hc78CKvgEeEK.png"</span></span>,
    "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
    "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
    "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
    </span>}</span>,
    "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
      "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
      </span>}
    </span>}</span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:06:12.000000Z"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:33:50.000000Z"</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-evento-post-1" class="action post"><h4 class="action-heading"><div class="name">Agregar o editar Imagen</div><a href="#servicios-de-viciny-evento-post-1" class="method post">POST</a><code class="uri">/event/{event_id}/image</code></h4><p>Servicio que permite agregar o actualizar la imagen del evento.
la imagen se pasa por form-data.</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/event/<span class="hljs-attribute" title="event_id">event_id</span>/image</span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>event_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del evento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/form-data</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    "<span class="hljs-attribute">image</span>":<span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">required</span>":<span class="hljs-value">[
        <span class="hljs-string">"image"</span>,
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">image</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"form-data"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa la imagen del evento"</span>
        </span>}
    </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
    "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
    "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"direccion"</span></span>,
    "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
    "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
    "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 10:55"</span></span>,
    "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
    "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/04/12 11:55"</span></span>,
    "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/storage/events/q0yVjMlb8LwYfi2LJUvLxRpI1X50hc78CKvgEeEK.png"</span></span>,
    "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
    "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
    "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
      "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
    </span>}</span>,
    "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
      "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
      "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
      </span>}
    </span>}</span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:06:12.000000Z"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:33:50.000000Z"</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">image</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo image es obligatorio."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-evento-get-2" class="action get"><h4 class="action-heading"><div class="name">Obtener eventos populares</div><a href="#servicios-de-viciny-evento-get-2" class="method get">GET</a><code class="uri">/event/popular</code></h4><p>Servicio el cual permite obtener los 3 eventos más pupulares</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/event/popular</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Fiesta Perdomo"</span></span>,
      "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
      "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
      "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
      "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">20000</span></span>,
      "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/storage/events/q0yVjMlb8LwYfi2LJUvLxRpI1X50hc78CKvgEeEK.png"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
      "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
      "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Otro"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">65</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
        "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ANTIOQUIA"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:00"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:00"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:06:12.000000Z"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:24:18.000000Z"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
      "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
      "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
      "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
      "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
      "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
      "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
        "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span>
    </span>}
  ]</span>,
  "<span class="hljs-attribute">links</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">first</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event?page=1"</span></span>,
    "<span class="hljs-attribute">last</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event?page=1"</span></span>,
    "<span class="hljs-attribute">prev</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">next</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>}</span>,
  "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">2</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-evento-post-2" class="action post"><h4 class="action-heading"><div class="name">Buscar eventos</div><a href="#servicios-de-viciny-evento-post-2" class="method post">POST</a><code class="uri">/event/search</code></h4><p>Servicio el cual permite buscar eventos</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/event/search</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020/01/22"</span></span>,
  "<span class="hljs-attribute">municipality_name</span>": <span class="hljs-value"><span class="hljs-string">""</span></span>,
  "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-string">"3"</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">optional</span>":<span class="hljs-value">[
        <span class="hljs-string">"start_date"</span>,
        <span class="hljs-string">"finish_date"</span>,
        <span class="hljs-string">"municipality"</span>,
        <span class="hljs-string">"name"</span>,
        <span class="hljs-string">"organizer"</span>,
        <span class="hljs-string">"type_event_id"</span>,
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">start_date</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"date"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el inicio del rango de la fecha"</span>
        </span>}</span>,
        "<span class="hljs-attribute">finish_date</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"date"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el final del rangode la fecha"</span>
        </span>}</span>,
        "<span class="hljs-attribute">municipality</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el nombre del municipio"</span>
        </span>}</span>,
        "<span class="hljs-attribute">name</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el nombre del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">organizer</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"text"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el nombre de los organizadores"</span>
        </span>}</span>,
        "<span class="hljs-attribute">type_event_id</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identificador de tipo de evento"</span>
        </span>}</span>,
    }
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Fiesta Perdomo"</span></span>,
      "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
      "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
      "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
      "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">20000</span></span>,
      "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/storage/events/q0yVjMlb8LwYfi2LJUvLxRpI1X50hc78CKvgEeEK.png"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
      "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
      "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Otro"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">65</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
        "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ANTIOQUIA"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:00"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:00"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:06:12.000000Z"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:24:18.000000Z"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
      "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
      "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
      "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
      "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
        "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span>
    </span>}
  ]</span>,
  "<span class="hljs-attribute">links</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">first</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event?page=1"</span></span>,
    "<span class="hljs-attribute">last</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event?page=1"</span></span>,
    "<span class="hljs-attribute">prev</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">next</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>}</span>,
  "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">2</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-evento-get-3" class="action get"><h4 class="action-heading"><div class="name">Obtener mis eventos creados</div><a href="#servicios-de-viciny-evento-get-3" class="method get">GET</a><code class="uri">/event/my-events</code></h4><p>Servicio el cual permite obtener los eventos creados</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/event/my-events</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Fiesta Perdomo"</span></span>,
      "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
      "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
      "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
      "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">20000</span></span>,
      "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/storage/events/q0yVjMlb8LwYfi2LJUvLxRpI1X50hc78CKvgEeEK.png"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
      "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
      "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Otro"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">65</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
        "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ANTIOQUIA"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:00"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:00"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:06:12.000000Z"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:24:18.000000Z"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
      "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
      "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
      "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
      "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
      "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
      "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
        "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span>
    </span>}
  ]</span>,
  "<span class="hljs-attribute">links</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">first</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event?page=1"</span></span>,
    "<span class="hljs-attribute">last</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event?page=1"</span></span>,
    "<span class="hljs-attribute">prev</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">next</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>}</span>,
  "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/event"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">2</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="servicios-de-viciny-eventos-favoritos" class="resource"><h3 class="resource-heading">Eventos favoritos <a href="#servicios-de-viciny-eventos-favoritos" class="permalink">&nbsp;&para;</a></h3><p>Lista de servicios para el control de los eventos favoritos.</p>
<div id="servicios-de-viciny-eventos-favoritos-get" class="action get"><h4 class="action-heading"><div class="name">Obtener todo los eventos favoritos</div><a href="#servicios-de-viciny-eventos-favoritos-get" class="method get">GET</a><code class="uri">/favorite</code></h4><p>Servicio el cual permite obtener todo los  eventos favoritos</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/favorite</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
      "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"direccion"</span></span>,
      "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
      "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
      "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"http://viciny.test/storage/events/qWIwGeMTjVzcHipMR5UtCvXJbUrKevrLA9QN17hq.png"</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
      "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
      "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
        "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:06:12.000000Z"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:42:43.000000Z"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ciencia Goma"</span></span>,
      "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"Perdomos"</span></span>,
      "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"google direccion"</span></span>,
      "<span class="hljs-attribute">longitude</span>": <span class="hljs-value"><span class="hljs-number">53.523713</span></span>,
      "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">0.357708</span></span>,
      "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">numberTickets</span>": <span class="hljs-value"><span class="hljs-number">44</span></span>,
      "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-04-12"</span></span>,
      "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descripcion del evento"</span></span>,
      "<span class="hljs-attribute">url_video</span>": <span class="hljs-value"><span class="hljs-string">"https://www.youtube.com/watch?v=pAgnJDJN4VA"</span></span>,
      "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Negocios"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:04:58"</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
        "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:05"</span></span>,
        "<span class="hljs-attribute">department</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"QUINDIO"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 18:05:01"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15T20:27:43.000000Z"</span>
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-eventos-favoritos-post" class="action post"><h4 class="action-heading"><div class="name">Agregar evento favorito</div><a href="#servicios-de-viciny-eventos-favoritos-post" class="method post">POST</a><code class="uri">/favorite</code></h4><p>Servicio el cual permite agregar un evento como favorito.</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/favorite</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">event_id</span>": <span class="hljs-value"><span class="hljs-number">4</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
        <span class="hljs-string">"event_id"</span>,
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">event_id</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identificador del evento"</span>
        </span>}
    </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">event_id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
  "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
  "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 20:48:49"</span></span>,
  "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 20:48:49"</span></span>,
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">event_id</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo event id es obligatorio."</span>
    ]</span>,
    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value">[
      <span class="hljs-string">"La combinación de user id, event id ya existe."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-eventos-favoritos-delete" class="action delete"><h4 class="action-heading"><div class="name">Eliminar evento favorito</div><a href="#servicios-de-viciny-eventos-favoritos-delete" class="method delete">DELETE</a><code class="uri">/favorite/{event_id}</code></h4><p>Servicio que permite eliminar el evento de favoritos</p>
<h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname"></span>/favorite/<span class="hljs-attribute" title="event_id">event_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>event_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del evento.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="servicios-de-viciny-ticket" class="resource"><h3 class="resource-heading">Ticket <a href="#servicios-de-viciny-ticket" class="permalink">&nbsp;&para;</a></h3><p>Lista de servicios para el control de los ticket (llaves de ventos).</p>
<div id="servicios-de-viciny-ticket-get" class="action get"><h4 class="action-heading"><div class="name">Obtener todo los tickets del usuario</div><a href="#servicios-de-viciny-ticket-get" class="method get">GET</a><code class="uri">/ticket</code></h4><p>Servicio el cual permite obtener todo los tickets del usuario</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/ticket</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">101</span></span>,
      "<span class="hljs-attribute">event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">205</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
        "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"quiceno"</span></span>,
        "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"La gran colombia"</span></span>,
        "<span class="hljs-attribute">longitude</span>": <span class="hljs-value">-<span class="hljs-number">75.666671</span></span>,
        "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">4.539262</span></span>,
        "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">22</span></span>,
        "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">5000</span></span>,
        "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-21 18:39:00"</span></span>,
        "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-24 18:39:00"</span></span>,
        "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"events/XnCCqlZfVOYPkh3yKBT0TW97364cXusk2jJeGD6E.png"</span></span>,
        "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descriptiva"</span></span>,
        "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">8</span></span>,
        "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-20 23:40:14"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-21 00:18:26"</span></span>,
        "<span class="hljs-attribute">by_delete</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Musica"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:21:19"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:21:19"</span></span>,
          "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
        </span>}</span>,
        "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
          "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:21:33"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:21:33"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">state</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">code_QR</span>": <span class="hljs-value"><span class="hljs-string">"iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAIAAAAP3aGbAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAIXklEQVR4nO3dwW4bORRFQdvI/3+yMYsAWTmMQHCod6Sq7cBSp6U56MUF9fn9/f0BUPD17AsAeJRgARmCBWQIFpAhWECGYAEZggVkCBaQIVhAhmABGYIFZAgWkCFYQIZgARmCBWQIFpAhWECGYAEZggVkCBaQIVhAhmABGYIFZAgWkCFYQIZgARmCBWQIFpAhWECGYAEZggVkCBaQIVhAhmABGYIFZAgWkCFYQIZgARmCBWQIFpAhWECGYAEZggVkCBaQIVhAhmABGYIFZAgWkCFYQIZgARm/nn0Bf/X1FY7p9/f3xl8t/sl7L7jnDS/j+Hu94bf3jvBtBd6NYAEZggVkCBaQIVhAhmABGYIFZAgWkDF3OLowZNi2Nw48Pince8HFPVz8p5sDy5svePMblf72Pl3yooH3JFhAhmABGYIFZAgWkCFYQIZgARmCBWQkh6MLx+dwx2d+e7PM4+vQPUPuxs0TRxeGXMbCkJHqQZ6wgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwg49WGo/Md3ygeP/j0+Hvt2Rs9Hl+iRk/mfFU+DCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyDAcve3mL7Df/Kvjbh6/OeSfzD95wgIyBAvIECwgQ7CADMECMgQLyBAsIEOwgIxXG47On/nd3CgOOad0yAvOX4cOuYzJPGEBGYIFZAgWkCFYQIZgARmCBWQIFpAhWEBGcjjq18Mf9KrnlN780fmb01b+yb0DMgQLyBAsIEOwgAzBAjIEC8gQLCBDsICMT4ccvrD0BHTh+Dp07724zxMWkCFYQIZgARmCBWQIFpAhWECGYAEZggVkzB2ODjmY8eZGcc/xK7x5GOneZey5+XkNOdD15gb4jhH/ywE8QrCADMECMgQLyBAsIEOwgAzBAjIEC8h4teHokLMo0+de3lyH3jR/YXvckMnuQdO/ZAB/CBaQIVhAhmABGYIFZAgWkCFYQIZgARlzh6MLx1d5Q3aeQxab6T3kkHNK59/DKE9YQIZgARmCBWQIFpAhWECGYAEZggVkCBaQ8evZF/BXx/eQQ34ifM/rHR35Pzl+o26eU7rnrZaonrCADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBj7nD05qGde4acRbl3GXvvNeSn6oecK3vc8Q/l9Uz/CAH+ECwgQ7CADMECMgQLyBAsIEOwgAzBAjLm/lT9zQNCj1/GcUMOPp3/T775XjdPHJ1/hXd4wgIyBAvIECwgQ7CADMECMgQLyBAsIEOwgIy5w9Hjbm5KjxvyMQ0ZIg5Z8+65ueZN36gfecICMgQLyBAsIEOwgAzBAjIEC8gQLCBDsICMuT9VP+SMzSFz0yHnrx538/NauPlRHv+85n/KB3nCAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CAjLnD0ZvecLC3d/E3f2b95gvuvdfCzW3zq35Ff+QJC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgxHPz7uHgJ5094VHl8bHr9RN9ehx+/hws0JaHRTOuL/K4BHCBaQIVhAhmABGYIFZAgWkCFYQIZgARnJ4eiQVd6QoyPn/zj7niHr0D2Tt5e/zb/CH3nCAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CAjM/ofmzDkJ3nkL9auHmj9sw/0HUhPb59uhEfPMAjBAvIECwgQ7CADMECMgQLyBAsIEOwgIzkiaM33VxsLhxfGw6Z0e694Pzbu3DzRkV/j37BExaQIVhAhmABGYIFZAgWkCFYQIZgARmCBWTMPXF0yKGdC+mzQ4dsL49/KMcNufNDpq1PN+I7AfAIwQIyBAvIECwgQ7CADMECMgQLyBAsIMOJox8fu/PFIYd27p3M+YYL2yFjzuP3cO+9Jq9DFzxhARmCBWQIFpAhWECGYAEZggVkCBaQIVhAxhsNR4fsPIecAjpkN7h3GUMGsXturpSHfMoHecICMgQLyBAsIEOwgAzBAjIEC8gQLCBDsICMNxqOLsw/znHIL8sff6/06aZ7hkxA54+Kf+QJC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMuYOR4+fRXncG85NjxuyDt2TXvNGuUFAhmABGYIFZAgWkCFYQIZgARmCBWQIFpAxdziaHtHdXGze3F7uveDxvzq+Dr15rOiQK5w/Kv5ROArAuxEsIEOwgAzBAjIEC8gQLCBDsIAMwQIy5g5HF4Zs3ob85vjx99rblN6cSt78Zfk9Q76iQy7joBGfLsAjBAvIECwgQ7CADMECMgQLyBAsIEOwgIzkcHRh/sGMN5eNQ6at848wXdjbyh435GjWp/OEBWQIFpAhWECGYAEZggVkCBaQIVhAhmABGa82HE3bO0hzyF8t7A0sh5wduufmmaivtw5dCH8ngHcjWECGYAEZggVkCBaQIVhAhmABGYIFZBiODjLkMNLjo8f0EaZ7l3FzfHtzA/x0nrCADMECMgQLyBAsIEOwgAzBAjIEC8gQLCDj1Yaj0Tncbzcvfv46dO+9bo45F27ejddbhy54wgIyBAvIECwgQ7CADMECMgQLyBAsIEOwgIzkcDT9I+bHDTmLcsiHcvPi0+8V3ZSO+JIBPEKwgAzBAjIEC8gQLCBDsIAMwQIyBAvI+Izux4A35AkLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgIz/AAZrCRDqYeztAAAAAElFTkSuQmCC"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">107</span></span>,
      "<span class="hljs-attribute">event</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">205</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
        "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"quiceno"</span></span>,
        "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"La gran colombia"</span></span>,
        "<span class="hljs-attribute">longitude</span>": <span class="hljs-value">-<span class="hljs-number">75.666671</span></span>,
        "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">4.539262</span></span>,
        "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">22</span></span>,
        "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">5000</span></span>,
        "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-21 18:39:00"</span></span>,
        "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-24 18:39:00"</span></span>,
        "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"events/XnCCqlZfVOYPkh3yKBT0TW97364cXusk2jJeGD6E.png"</span></span>,
        "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descriptiva"</span></span>,
        "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">8</span></span>,
        "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-20 23:40:14"</span></span>,
        "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-21 00:18:26"</span></span>,
        "<span class="hljs-attribute">by_delete</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">type_event</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Musica"</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:21:19"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:21:19"</span></span>,
          "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
        </span>}</span>,
        "<span class="hljs-attribute">municipality</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Armenia"</span></span>,
          "<span class="hljs-attribute">department_id</span>": <span class="hljs-value"><span class="hljs-number">63</span></span>,
          "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:21:33"</span></span>,
          "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-18 04:21:33"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">state</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">code_QR</span>": <span class="hljs-value"><span class="hljs-string">"iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAIAAAAP3aGbAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAIWUlEQVR4nO3dwW7cNhRA0SbI/3+y0UUAL4qWHRMM5135nL1nOJJ8ocUD+ePj4+MvgIKf714AwKsEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8j49e4F/KefP8Mx/fj42PirxU/e+8Cb9hZ//C4PufLf8Om9I3xZge9GsIAMwQIyBAvIECwgQ7CADMECMgQLyJg7OLowZLBtbzjw+Ejh3gfenIc8vsLFB+6NgN58otJP79slFw18T4IFZAgWkCFYQIZgARmCBWQIFpAhWEBGcnB0YcgOlnsfuDcPufddC0O+6+YWpjd/8vFlLAwZUj3IGxaQIVhAhmABGYIFZAgWkCFYQIZgARmCBWQ8bXB0vpvbbx5fxp6bB8HvXajjl5c/xM0AMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMg6O3zR+V3HNzLPPmdOjzNu1M84YFZAgWkCFYQIZgARmCBWQIFpAhWECGYAEZTxscnT/md3NG8eZGmjenQ/fMnw4dsozJvGEBGYIFZAgWkCFYQIZgARmCBWQIFpAhWEBGcnDU6eEvGnKk+/GZ0vnftfeB/C/XDsgQLCBDsIAMwQIyBAvIECwgQ7CADMECMn7Y5PDB5h9Vf/y7jvMPMoo3LCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyJg7ODpkY8abp73vmb/CPUN2N90zf0PXsf/4a+EHGvhuBAvIECwgQ7CADMECMgQLyBAsIEOwgIzk4OjN4cCb1+d5Y36/3bwpNx+b+RObx3/X23nDAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CAjLmDo3tuTgD6wBc/8LibY5lPnSmN8oYFZAgWkCFYQIZgARmCBWQIFpAhWECGYAEZ32hwdP4vHbJdanpwdM/xCzXkasx/5r/KGxaQIVhAhmABGYIFZAgWkCFYQIZgARmCBWT8evcCRhgyiLj3gXuLH7JP6Z6bA7E37Y2APm86dGH6LQT4JFhAhmABGYIFZAgWkCFYQIZgARmCBWQ8bcfRPfOPPl946qnoQ27K8VnZm7ubzt889qu8YQEZggVkCBaQIVhAhmABGYIFZAgWkCFYQMbcHUfnDz0e3x9yyEzpcXv36+ZdvrnCIRufDvkn+qoR1w7gFYIFZAgWkCFYQIZgARmCBWQIFpAhWEDG3MHRhfTo3c3FHx++Pb74ISsc8l3HtxWNTocujPjPB3iFYAEZggVkCBaQIVhAhmABGYIFZAgWkJE8qn7IpNyQZSwMOan85nTozcUvDDk+fv4j+lXesIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIGPu4KjhwHdJX/njyzh+lxdubka6t4y384YFZAgWkCFYQIZgARmCBWQIFpAhWECGYAEZc4+qnz9Ed3wZN0+xH3I4+95fzT/SffLs5W/zV/ivvGEBGYIFZAgWkCFYQIZgARmCBWQIFpAhWEDG3B1HFwxYvvhd8xe/t4ybu4Au3Ly8e4r/3WvesIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIGPujqMLN8fhbh5HftPx3zX/3PaFIcOcQzbFncwbFpAhWECGYAEZggVkCBaQIVhAhmABGYIFZMzdcfTmceQ3BxGHTL2Ove+fhuw4OuQBGDLa+nbesIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIONpg6N7H3jc2Kv6aciE7Z7jO3PeXMaeIfOrb+cNC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMpJH1e+5eUT4EPPnBvdWuHcrb855Ltxc/JC7fNCIWwjwCsECMgQLyBAsIEOwgAzBAjIEC8gQLCDjaTuO3pyvG3Iq+sL8qzH28fs0f9x0z/yh4n814mYAvEKwgAzBAjIEC8gQLCBDsIAMwQIyBAvImDs4+lRDxk2HzHnevBrHR5HTw5wGRwH+LMECMgQLyBAsIEOwgAzBAjIEC8gQLCBj7lH1Q3Z63LM3ejf/EPOb04Y3V3j8YTt+oYbMG79dOArAdyNYQIZgARmCBWQIFpAhWECGYAEZggVkzN1xdP6OiENWOGSbyoUhyzhu/u8a8p9y0IgbD/AKwQIyBAvIECwgQ7CADMECMgQLyBAsIGPujqN75o/emfP8o8u4eVT9zfnV+TvE3uENC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMp42OPpUx4cej//Vwt6A5ZCfvOfmdz1vOnTBGxaQIVhAhmABGYIFZAgWkCFYQIZgARmCBWQYHG24OQF4fOhxyBamNwdib37XzRngt/OGBWQIFpAhWECGYAEZggVkCBaQIVhAhmABGU8bHI2Ow/12fCxzyLjpniF7h+65eeWfNx26MOLuArxCsIAMwQIyBAvIECwgQ7CADMECMgQLyPgxdrRsyATgnvSmnTfPo18Ysk/pws0LdXOf0snCUQC+G8ECMgQLyBAsIEOwgAzBAjIEC8gQLCBj7uAowD94wwIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwgQ7CADMECMgQLyBAsIEOwgAzBAjIEC8gQLCBDsIAMwQIyBAvIECwg429TDvPyyA2P5wAAAABJRU5ErkJggg=="</span>
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-ticket-post" class="action post"><h4 class="action-heading"><div class="name">Comprar ticket</div><a href="#servicios-de-viciny-ticket-post" class="method post">POST</a><code class="uri">/ticket</code></h4><p>Servicio el cual permite comprar un ticket.</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/ticket</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    "<span class="hljs-attribute">event_id</span>":<span class="hljs-value"><span class="hljs-number">4</span>
    <span class="hljs-string">"tickes_quantity"</span>:<span class="hljs-number">1</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
    "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
        <span class="hljs-string">"event_id"</span>,
        <span class="hljs-string">"tickes_quantity"</span>
    ]</span>,
    "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">event_id</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el identificador del evento"</span>
        </span>}</span>,
        "<span class="hljs-attribute">tickes_quantity</span>":<span class="hljs-value">{
            "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"numeric"</span></span>,
            "<span class="hljs-attribute">description</span>":<span class="hljs-value"><span class="hljs-string">"representa el numero de tickets a comprar"</span>
        </span>}
    </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">event_id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
  "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
  "<span class="hljs-attribute">code</span>": <span class="hljs-value"><span class="hljs-string">"codigo"</span></span>,
  "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 21:17:22"</span></span>,
  "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-01-15 21:17:22"</span></span>,
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">event_id</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo event id es obligatorio."</span>
    ]</span>,
    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value">[
      <span class="hljs-string">"La combinación de user id, event id ya existe."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-ticket-post-1" class="action post"><h4 class="action-heading"><div class="name">Confirmar ticket de pago (payco)</div><a href="#servicios-de-viciny-ticket-post-1" class="method post">POST</a><code class="uri">/ticket/confirmation-pay</code></h4><p>Servicio el cual permite confirmar el pago por epayco.</p>
<h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname"></span>/ticket/confirmation-pay</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">x_cust_id_cliente</span>": <span class="hljs-value"><span class="hljs-string">"63111"</span></span>,
  "<span class="hljs-attribute">x_ref_payco</span>": <span class="hljs-value"><span class="hljs-string">"15363110"</span></span>,
  "<span class="hljs-attribute">x_id_factura</span>": <span class="hljs-value"><span class="hljs-string">"1f735e5f-f9c2-4d6a-912c-d2a1f58e0297"</span></span>,
  "<span class="hljs-attribute">x_id_invoice</span>": <span class="hljs-value"><span class="hljs-string">"1f735e5f-f9c2-4d6a-912c-d2a1f58e0297"</span></span>,
  "<span class="hljs-attribute">x_description</span>": <span class="hljs-value"><span class="hljs-string">"Viciny juan carlos Cantidad de llaves 1"</span></span>,
  "<span class="hljs-attribute">x_amount</span>": <span class="hljs-value"><span class="hljs-string">"5950"</span></span>,
  "<span class="hljs-attribute">x_amount_country</span>": <span class="hljs-value"><span class="hljs-string">"5950"</span></span>,
  "<span class="hljs-attribute">x_amount_ok</span>": <span class="hljs-value"><span class="hljs-string">"5950"</span></span>,
  "<span class="hljs-attribute">x_tax</span>": <span class="hljs-value"><span class="hljs-string">"950"</span></span>,
  "<span class="hljs-attribute">x_amount_base</span>": <span class="hljs-value"><span class="hljs-string">"5000"</span></span>,
  "<span class="hljs-attribute">x_currency_code</span>": <span class="hljs-value"><span class="hljs-string">"COP"</span></span>,
  "<span class="hljs-attribute">x_bank_name</span>": <span class="hljs-value"><span class="hljs-string">"Banco de Pruebas"</span></span>,
  "<span class="hljs-attribute">x_cardnumber</span>": <span class="hljs-value"><span class="hljs-string">"457562*******0326"</span></span>,
  "<span class="hljs-attribute">x_quotas</span>": <span class="hljs-value"><span class="hljs-string">"1"</span></span>,
  "<span class="hljs-attribute">x_respuesta</span>": <span class="hljs-value"><span class="hljs-string">"Aceptada"</span></span>,
  "<span class="hljs-attribute">x_response</span>": <span class="hljs-value"><span class="hljs-string">"Aceptada"</span></span>,
  "<span class="hljs-attribute">x_approval_code</span>": <span class="hljs-value"><span class="hljs-string">"000000"</span></span>,
  "<span class="hljs-attribute">x_transaction_id</span>": <span class="hljs-value"><span class="hljs-string">"15363110"</span></span>,
  "<span class="hljs-attribute">x_fecha_transaccion</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-20 18:43:11"</span></span>,
  "<span class="hljs-attribute">x_transaction_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-20 18:43:11"</span></span>,
  "<span class="hljs-attribute">x_cod_respuesta</span>": <span class="hljs-value"><span class="hljs-string">"1"</span></span>,
  "<span class="hljs-attribute">x_cod_response</span>": <span class="hljs-value"><span class="hljs-string">"1"</span></span>,
  "<span class="hljs-attribute">x_response_reason_text</span>": <span class="hljs-value"><span class="hljs-string">"00-Aprobada"</span></span>,
  "<span class="hljs-attribute">x_errorcode</span>": <span class="hljs-value"><span class="hljs-string">"00"</span></span>,
  "<span class="hljs-attribute">x_cod_transaction_state</span>": <span class="hljs-value"><span class="hljs-string">"1"</span></span>,
  "<span class="hljs-attribute">x_transaction_state</span>": <span class="hljs-value"><span class="hljs-string">"Aceptada"</span></span>,
  "<span class="hljs-attribute">x_franchise</span>": <span class="hljs-value"><span class="hljs-string">"VS"</span></span>,
  "<span class="hljs-attribute">x_business</span>": <span class="hljs-value"><span class="hljs-string">"JUAN CARLOS PERDOMO QUICENO"</span></span>,
  "<span class="hljs-attribute">x_customer_doctype</span>": <span class="hljs-value"><span class="hljs-string">"CC"</span></span>,
  "<span class="hljs-attribute">x_customer_document</span>": <span class="hljs-value"><span class="hljs-string">"1094919629"</span></span>,
  "<span class="hljs-attribute">x_customer_name</span>": <span class="hljs-value"><span class="hljs-string">"JUAN CARLOS PERDOMO"</span></span>,
  "<span class="hljs-attribute">x_customer_lastname</span>": <span class="hljs-value"><span class="hljs-string">"QUICENO"</span></span>,
  "<span class="hljs-attribute">x_customer_email</span>": <span class="hljs-value"><span class="hljs-string">"jcperdomoq@uqvirtual.edu.co"</span></span>,
  "<span class="hljs-attribute">x_customer_phone</span>": <span class="hljs-value"><span class="hljs-string">"0000000"</span></span>,
  "<span class="hljs-attribute">x_customer_movil</span>": <span class="hljs-value"><span class="hljs-string">"3023537918"</span></span>,
  "<span class="hljs-attribute">x_customer_ind_pais</span>": <span class="hljs-value"><span class="hljs-string">"57"</span></span>,
  "<span class="hljs-attribute">x_customer_country</span>": <span class="hljs-value"><span class="hljs-string">"CO"</span></span>,
  "<span class="hljs-attribute">x_customer_city</span>": <span class="hljs-value"><span class="hljs-string">"armenia"</span></span>,
  "<span class="hljs-attribute">x_customer_address</span>": <span class="hljs-value"><span class="hljs-string">"la rivera"</span></span>,
  "<span class="hljs-attribute">x_customer_ip</span>": <span class="hljs-value"><span class="hljs-string">"186.115.243.253"</span></span>,
  "<span class="hljs-attribute">x_signature</span>": <span class="hljs-value"><span class="hljs-string">"138a79884cf2b6dfe213133f91e3c88dda94a97be792acd44a4bad6c9d2d8e8c"</span></span>,
  "<span class="hljs-attribute">x_test_request</span>": <span class="hljs-value"><span class="hljs-string">"TRUE"</span>
</span>}</code></pre><div style="height: 1px;"></div><h5>Schema</h5><pre><code>{
  "<span class="hljs-attribute">required</span>": <span class="hljs-value">[
    <span class="hljs-string">"event_id"</span>,
    <span class="hljs-string">"tickes_quantity"</span>
  ]</span>,
  "<span class="hljs-attribute">properties</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">epayco_developer</span>": <span class="hljs-value"><span class="hljs-string">"son las varibles de epayco en json"</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-ticket-get-1" class="action get"><h4 class="action-heading"><div class="name">Verificar ticket</div><a href="#servicios-de-viciny-ticket-get-1" class="method get">GET</a><code class="uri">/ticket/check/{code}</code></h4><p>Servicio el cual permite verificar la validacion del ticket</p>
<h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname"></span>/ticket/check/<span class="hljs-attribute" title="code">code</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>code</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>codigo del ticket.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">101</span></span>,
  "<span class="hljs-attribute">tickets_order_id</span>": <span class="hljs-value"><span class="hljs-number">13</span></span>,
  "<span class="hljs-attribute">state</span>": <span class="hljs-value"><span class="hljs-literal">true</span></span>,
  "<span class="hljs-attribute">code</span>": <span class="hljs-value"><span class="hljs-string">"6c19c9d7-bb10-4c9e-945e-8b2072"</span></span>,
  "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-22 01:12:03"</span></span>,
  "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-22 15:19:06"</span></span>,
  "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
  "<span class="hljs-attribute">event</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">205</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"juan carlos"</span></span>,
    "<span class="hljs-attribute">organizer</span>": <span class="hljs-value"><span class="hljs-string">"quiceno"</span></span>,
    "<span class="hljs-attribute">address</span>": <span class="hljs-value"><span class="hljs-string">"La gran colombia"</span></span>,
    "<span class="hljs-attribute">longitude</span>": <span class="hljs-value">-<span class="hljs-number">75.666671</span></span>,
    "<span class="hljs-attribute">latitude</span>": <span class="hljs-value"><span class="hljs-number">4.539262</span></span>,
    "<span class="hljs-attribute">tickets</span>": <span class="hljs-value"><span class="hljs-number">22</span></span>,
    "<span class="hljs-attribute">ticket_value</span>": <span class="hljs-value"><span class="hljs-number">5000</span></span>,
    "<span class="hljs-attribute">start_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-21 18:39:00"</span></span>,
    "<span class="hljs-attribute">finish_date</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-24 18:39:00"</span></span>,
    "<span class="hljs-attribute">image</span>": <span class="hljs-value"><span class="hljs-string">"events/XnCCqlZfVOYPkh3yKBT0TW97364cXusk2jJeGD6E.png"</span></span>,
    "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"descriptiva"</span></span>,
    "<span class="hljs-attribute">type_event_id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">11</span></span>,
    "<span class="hljs-attribute">municipality_id</span>": <span class="hljs-value"><span class="hljs-number">66</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-20 23:40:14"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-21 00:18:26"</span></span>,
    "<span class="hljs-attribute">by_delete</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>}</span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">11</span></span>,
    "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Juan carlos"</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"jcpq60981@hotmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-19 19:12:52"</span></span>,
    "<span class="hljs-attribute">password</span>": <span class="hljs-value"><span class="hljs-string">"$2y$10$dyeX748LceJLJVOWmZefIO2sqkx3HhndVDWqaTAtwIxKBEeAFU/2K"</span></span>,
    "<span class="hljs-attribute">remember_token</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-19 19:12:30"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-19 19:12:52"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>403</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"La llave no pertenece a este evento."</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div><div id="servicios-de-viciny-ticket-delete" class="action delete"><h4 class="action-heading"><div class="name">Eliminar ticket</div><a href="#servicios-de-viciny-ticket-delete" class="method delete">DELETE</a><code class="uri">/ticket/{ticket_id}</code></h4><p>Servicio el cual permite eliminar un ticket.</p>
<h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname"></span>/ticket/<span class="hljs-attribute" title="ticket_id">ticket_id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>ticket_id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>identificador del ticket.</p>
</dd></dl></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Accept</span>: <span class="hljs-string">application/json</span><br><span class="hljs-attribute">Authorization</span>: <span class="hljs-string">Bearer</span></code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">101</span></span>,
  "<span class="hljs-attribute">tickets_order_id</span>": <span class="hljs-value"><span class="hljs-number">13</span></span>,
  "<span class="hljs-attribute">state</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
  "<span class="hljs-attribute">code</span>": <span class="hljs-value"><span class="hljs-string">"6c19c9d7-bb10-4c9e-945e-8b207289bd38"</span></span>,
  "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-22 01:12:03"</span></span>,
  "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-22 14:48:29"</span></span>,
  "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-02-22 14:48:29"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>400</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Token ausente"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>422</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"The given data was invalid."</span></span>,
  "<span class="hljs-attribute">errors</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">event_id</span>": <span class="hljs-value">[
      <span class="hljs-string">"El campo event id es obligatorio."</span>
    ]</span>,
    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value">[
      <span class="hljs-string">"La combinación de user id, event id ya existe."</span>
    ]
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div></section></div></div></div><p style="text-align: center;" class="text-muted">Generated by&nbsp;<a href="https://github.com/danielgtaylor/aglio" class="aglio">aglio</a>&nbsp;on 22 Feb 2020</p><script>/* eslint-env browser */
/* eslint quotes: [2, "single"] */
'use strict';

/*
  Determine if a string ends with another string.
*/
function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

/*
  Get a list of direct child elements by class name.
*/
function childrenByClass(element, name) {
  var filtered = [];

  for (var i = 0; i < element.children.length; i++) {
    var child = element.children[i];
    var classNames = child.className.split(' ');
    if (classNames.indexOf(name) !== -1) {
      filtered.push(child);
    }
  }

  return filtered;
}

/*
  Get an array [width, height] of the window.
*/
function getWindowDimensions() {
  var w = window,
      d = document,
      e = d.documentElement,
      g = d.body,
      x = w.innerWidth || e.clientWidth || g.clientWidth,
      y = w.innerHeight || e.clientHeight || g.clientHeight;

  return [x, y];
}

/*
  Collapse or show a request/response example.
*/
function toggleCollapseButton(event) {
    var button = event.target.parentNode;
    var content = button.parentNode.nextSibling;
    var inner = content.children[0];

    if (button.className.indexOf('collapse-button') === -1) {
      // Clicked without hitting the right element?
      return;
    }

    if (content.style.maxHeight && content.style.maxHeight !== '0px') {
        // Currently showing, so let's hide it
        button.className = 'collapse-button';
        content.style.maxHeight = '0px';
    } else {
        // Currently hidden, so let's show it
        button.className = 'collapse-button show';
        content.style.maxHeight = inner.offsetHeight + 12 + 'px';
    }
}

function toggleTabButton(event) {
    var i, index;
    var button = event.target;

    // Get index of the current button.
    var buttons = childrenByClass(button.parentNode, 'tab-button');
    for (i = 0; i < buttons.length; i++) {
        if (buttons[i] === button) {
            index = i;
            button.className = 'tab-button active';
        } else {
            buttons[i].className = 'tab-button';
        }
    }

    // Hide other tabs and show this one.
    var tabs = childrenByClass(button.parentNode.parentNode, 'tab');
    for (i = 0; i < tabs.length; i++) {
        if (i === index) {
            tabs[i].style.display = 'block';
        } else {
            tabs[i].style.display = 'none';
        }
    }
}

/*
  Collapse or show a navigation menu. It will not be hidden unless it
  is currently selected or `force` has been passed.
*/
function toggleCollapseNav(event, force) {
    var heading = event.target.parentNode;
    var content = heading.nextSibling;
    var inner = content.children[0];

    if (heading.className.indexOf('heading') === -1) {
      // Clicked without hitting the right element?
      return;
    }

    if (content.style.maxHeight && content.style.maxHeight !== '0px') {
      // Currently showing, so let's hide it, but only if this nav item
      // is already selected. This prevents newly selected items from
      // collapsing in an annoying fashion.
      if (force || window.location.hash && endsWith(event.target.href, window.location.hash)) {
        content.style.maxHeight = '0px';
      }
    } else {
      // Currently hidden, so let's show it
      content.style.maxHeight = inner.offsetHeight + 12 + 'px';
    }
}

/*
  Refresh the page after a live update from the server. This only
  works in live preview mode (using the `--server` parameter).
*/
function refresh(body) {
    document.querySelector('body').className = 'preload';
    document.body.innerHTML = body;

    // Re-initialize the page
    init();
    autoCollapse();

    document.querySelector('body').className = '';
}

/*
  Determine which navigation items should be auto-collapsed to show as many
  as possible on the screen, based on the current window height. This also
  collapses them.
*/
function autoCollapse() {
  var windowHeight = getWindowDimensions()[1];
  var itemsHeight = 64; /* Account for some padding */
  var itemsArray = Array.prototype.slice.call(
    document.querySelectorAll('nav .resource-group .heading'));

  // Get the total height of the navigation items
  itemsArray.forEach(function (item) {
    itemsHeight += item.parentNode.offsetHeight;
  });

  // Should we auto-collapse any nav items? Try to find the smallest item
  // that can be collapsed to show all items on the screen. If not possible,
  // then collapse the largest item and do it again. First, sort the items
  // by height from smallest to largest.
  var sortedItems = itemsArray.sort(function (a, b) {
    return a.parentNode.offsetHeight - b.parentNode.offsetHeight;
  });

  while (sortedItems.length && itemsHeight > windowHeight) {
    for (var i = 0; i < sortedItems.length; i++) {
      // Will collapsing this item help?
      var itemHeight = sortedItems[i].nextSibling.offsetHeight;
      if ((itemsHeight - itemHeight <= windowHeight) || i === sortedItems.length - 1) {
        // It will, so let's collapse it, remove its content height from
        // our total and then remove it from our list of candidates
        // that can be collapsed.
        itemsHeight -= itemHeight;
        toggleCollapseNav({target: sortedItems[i].children[0]}, true);
        sortedItems.splice(i, 1);
        break;
      }
    }
  }
}

/*
  Initialize the interactive functionality of the page.
*/
function init() {
    var i, j;

    // Make collapse buttons clickable
    var buttons = document.querySelectorAll('.collapse-button');
    for (i = 0; i < buttons.length; i++) {
        buttons[i].onclick = toggleCollapseButton;

        // Show by default? Then toggle now.
        if (buttons[i].className.indexOf('show') !== -1) {
            toggleCollapseButton({target: buttons[i].children[0]});
        }
    }

    var responseCodes = document.querySelectorAll('.example-names');
    for (i = 0; i < responseCodes.length; i++) {
        var tabButtons = childrenByClass(responseCodes[i], 'tab-button');
        for (j = 0; j < tabButtons.length; j++) {
            tabButtons[j].onclick = toggleTabButton;

            // Show by default?
            if (j === 0) {
                toggleTabButton({target: tabButtons[j]});
            }
        }
    }

    // Make nav items clickable to collapse/expand their content.
    var navItems = document.querySelectorAll('nav .resource-group .heading');
    for (i = 0; i < navItems.length; i++) {
        navItems[i].onclick = toggleCollapseNav;

        // Show all by default
        toggleCollapseNav({target: navItems[i].children[0]});
    }
}

// Initial call to set up buttons
init();

window.onload = function () {
    autoCollapse();
    // Remove the `preload` class to enable animations
    document.querySelector('body').className = '';
};
</script></body></html>