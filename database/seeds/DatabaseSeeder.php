<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(TypeEventTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(MunicipalityTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        factory(\App\Models\User::class, 1)->create();
        factory(\App\Models\Event::class, 200)->create();

    }
}
