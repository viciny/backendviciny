<?php

use Illuminate\Database\Seeder;
use \App\Models\User;
use \Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //crear user root
        $user = new User([
            'name'=>"Admin viciny",
            "email"=>"vicinyapp@gmail.com",
            "password"=>bcrypt('JUa7kLw2Mz5yxGG'),
            "email_verified_at"=>now()
        ]);
        $user->save();

        $role = Role::create(['name' => 'administrator']);
        // Asignación del rol
        $user->assignRole($role);
    }
}
