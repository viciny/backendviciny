<?php

use Illuminate\Database\Seeder;
use \App\Models\TypeEvent;
class TypeEventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = $this->typeEvents();
        foreach ($types as $type){
            TypeEvent::create($type);
        }
    }

    public function typeEvents()
    {
        return [
            ['name' => 'Otro'],
            ['name' => 'Negocios'],
            ['name' => 'Ciencia y tecnología'],
            ['name' => 'Musica'],
            ['name' => 'Cine y medios de comunicación'],
            ['name' => 'Artes escénicas y visuales'],
            ['name' => 'Moda'],
            ['name' => 'Salud'],
            ['name' => 'Deportes y salud'],
            ['name' => 'Viajes y actividades al aire libre'],
            ['name' => 'Gastronomía'],
            ['name' => 'Solidaridad'],
            ['name' => 'Gobierno'],
            ['name' => 'Comunidad'],
            ['name' => 'Espiritualidad'],
            ['name' => 'Famllia y educación'],
            ['name' => 'Feriados'],
            ['name' => 'Hogar y estilo de vida'],
            ['name' => 'Coches, barcos y aviones'],
            ['name' => 'Aficiones'],
            ['name' => 'Actividades escolares'],
        ];
    }
}
