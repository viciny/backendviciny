<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTypeEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_type_event', function (Blueprint $table) {
            $table->unsignedBigInteger('type_event_id');
            $table->unsignedBigInteger('user_id');
            $table->unique(['type_event_id','user_id']);

            //key foreign
            $table->foreign('type_event_id')->references('id')->on('type_events')
                ->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_type_event');
    }
}
