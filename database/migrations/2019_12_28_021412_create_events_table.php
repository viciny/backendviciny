<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name");
            $table->string("organizer");
            $table->string('address');
            $table->double("longitude");
            $table->double("latitude");
            $table->integer('tickets')->unsigned();
            $table->double('ticket_value')->default(0);
            $table->dateTime('start_date', 0);
            $table->dateTime('finish_date', 0);
            $table->string("image")->nullable();
            $table->text("description")->nullable();
            $table->unsignedBigInteger('type_event_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('municipality_id')->nullable();
            $table->timestamps();
            $table->boolean('by_delete')->default(false);
            $table->softDeletes();
            //key foreign
            $table->foreign('type_event_id')->references('id')->on('type_events')
                ->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('municipality_id')->references('id')->on('municipalities')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
