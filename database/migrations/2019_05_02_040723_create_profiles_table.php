<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type_document',['cc','ti','tp','rc','ce','ci','dni'])->nullable();
            $table->string("nit")->nullable()->unique();
            $table->text('photo')->nullable();
            $table->string('phone')->nullable();
            $table->string("bank_name")->nullable();
            $table->string("account_bank")->nullable();
            $table->string("address")->nullable();
            $table->unsignedBigInteger('municipality_id')->nullable();
            $table->unsignedBigInteger('user_id')->unique();
            $table->timestamps();

            //key foreign
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('municipality_id')->references('id')->on('municipalities')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
