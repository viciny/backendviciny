<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use \Carbon\Carbon;

$factory->define(\App\Models\Event::class, function (Faker $faker) {
    return [
        'name'=>$faker->firstName,
        'organizer'=>$faker->company,
        'address'=>$faker->streetAddress,
        'longitude'=>$faker->longitude($min = -76, $max = -72),
        'latitude'=>$faker->latitude($min = 2, $max = 6),
        'tickets'=>$faker->numberBetween($min = 1, $max = 90),
        'ticket_value'=>$faker->numberBetween($min = 1000, $max = 90000),
        'start_date'=>(Carbon::now())->add(rand ( 0, 15),'day' ),
        'finish_date'=>(Carbon::now())->add(rand ( 16, 30),'day' ),
        'description'=>$faker->text($maxNbChars = 200),
        'image'=>'events/tU8ufZXSjdk56Zw4Xah7KgzKKDnDfd7WQb1cfyNb.png',
        'type_event_id'=>$faker->numberBetween($min = 1, $max = 20),
        'municipality_id'=>$faker->randomElement($array = array ('66','107','547','532')),
        'user_id'=>1
    ];
});
